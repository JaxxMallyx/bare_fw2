<?php

spl_autoload_register( function( $className ) {

    $appArr = array('admin', 'ajax', 'base', 'ext');
    $file = $className . '.php';
    $fileArr = explode('\\', $file);

    if ( !empty(array_intersect($appArr, $fileArr)) ) {
        $file = __DIR__ . '\\App\\' . $file;
    } else {
        $file = __DIR__ . '\\' . $file;
    }

    if ( file_exists( $file ) ) {
        require_once $file;
    } else {
        if (\base\controllers\ApplicationController::STATE == 'DEVELOPMENT' ) {
            throw new Exception('Unable to load object file for '.$className.'. Please check namespaces, directories and filenames.');
        } else {
            \base\controllers\ApplicationController::go_to( '404' );
        }
    }
});