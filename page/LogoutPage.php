<?php

namespace page;

class LogoutPage extends \page\PageAbstract {

    public function __construct( $pageObj )
    {
        $this->set_pageObj( $pageObj );

        session_destroy();

        \base\controllers\ApplicationController::go_to( 'home' );

    }
}