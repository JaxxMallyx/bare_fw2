<?php

namespace page;

class RegisterPage extends \page\PageAbstract {

    private $form;

    public $formElement;

    private $formValues;

    public function __construct( $pageObj )
    {
        $this->set_pageObj( $pageObj );

        if ( $this->pageObj->userObj !== false ) {
            $this->pageObj->routeObj->go_to( 'dashboard' );
        }

        $this->form = new \base\controllers\FormController('register');
        $this->form->set_input('text', 'username', array( 'required' => true ) );
        $this->form->set_input('email', 'email', array( 'required' => true ) );
        $this->form->set_input('password', 'password', array( 'required' => true ) );
        $this->form->set_input('text', 'firstname', array( 'required' => true ) );
        $this->form->set_input('text', 'prefix' );
        $this->form->set_input('text', 'lastname', array( 'required' => true ) );
        $this->form->set_input('submit', 'register' );

        $this->formElement = $this->form->generate_form();

        $this->formValues = $this->form->get_formValues();

        if ( !empty( $this->formValues ) ) $this->register_form_handler();
    }

    /*
     * private register_form_handler method
     * @param array $postArr
     */
    private function register_form_handler( )
    {
        $username = $this->form->get_formValues( 'username', false );
        $email = $this->form->get_formValues( 'email', false );
        $password = $this->form->get_formValues( 'password', false );
        $firstname = $this->form->get_formValues( 'firstname', false );
        $prefix = $this->form->get_formValues( 'prefix', false );
        $lastname = $this->form->get_formValues( 'lastname', false );

        $user = new \base\controllers\UserController( 0, $username, $email, $password, $firstname, $prefix, $lastname);

        if ( $user->register_user() == true ) {
            \base\controllers\ApplicationController::go_to( 'login', array( 'state' => 'register-success' ) );
        } else {
            $this->form->set_formFeedback( 'Username or email already exists, try again' );
        }

    }

}