<?php

namespace page;

class HomePage extends \page\PageAbstract {

    public function __construct( $pageObj )
    {
        $this->set_pageObj( $pageObj );
        $this->pageObj->set_dependencies('footer', 'home', 'css');
    }

    protected function get_rights()
    {

    }
}