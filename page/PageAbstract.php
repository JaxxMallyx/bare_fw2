<?php

namespace page;

abstract class PageAbstract {

    public $pageObj;

    public function set_pageObj( object $pageObj )
    {
        $this->pageObj = $pageObj;
    }
}