<?php

namespace page;

class LoginPage extends \page\PageAbstract {

    private $form;

    public $formElement;

    private $formValues;

    public function __construct( $pageObj )
    {
        $this->set_pageObj( $pageObj );

        if ( $this->pageObj->userObj !== false ) {
            $this->pageObj->routeObj->go_to( 'dashboard' );
        }

        $this->form = new \base\controllers\FormController('login');
        $this->form->set_input('text', 'username', array( 'required' => true ) );
        $this->form->set_input('password', 'password', array( 'required' => true ) );
        $this->form->set_input('submit', 'inloggen');

        $this->formElement = $this->form->generate_form();

        $this->formValues = $this->form->get_formValues();

        $this->login_form_handler();
    }

    /*
     * private login_form_handler method
     * @param array $postArr
     */
    private function login_form_handler()
    {
        $username = $this->form->get_formValues( 'username', false );
        $password = $this->form->get_formValues( 'password', false );

        if ( !empty( $username ) && !empty( $password ) ) {
            $user = new \base\controllers\UserController(0, $username, '', $password);
            if ( $user->login_user() ) {
                \base\controllers\ApplicationController::go_to( 'dashboard' );
            } else {
                $this->form->set_formFeedback( 'Username-password combination is wrong, try again.');
            }
        }
    }

}