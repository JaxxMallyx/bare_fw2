( function( $ ) {

    $('div.background > svg').addClass('active');
    set_menu_classes( $(window).outerWidth() );
    set_table_labels();

    $(window).on('resize', function() {
        var viewWidth = $(this).outerWidth();
        console.log($(window).outerWidth());
        console.log($(window).outerHeight());

        set_menu_classes( viewWidth );
    });

    $(window).on('keydown', function(e) {
        var key = e.key;

        if (key == 'Escape') {
            if ($('div.lb-container.active').length > 0) {
                close_bflb();
            }
        }
    });

    $('div.user-menu-container>a.user-title').click(function() {
        $(this).toggleClass('active');
        $('ul.user-menu').toggleClass('active');
    });

    $('nav div.menu-container div.menu-icon').click(function() {
        $(this).children('div.hamburger').toggleClass('is-active');
        $(this).parents('div.menu-container').toggleClass('open');
        $('body main').toggleClass('open-menu');
    });

    $('div.lb-ct-container>div.close-lb').click(function() {
        close_bflb();
    });

    $('body').on('submit', 'div.ct>form', function(e) {
        e.preventDefault();
        var inputArr = gen_input_array($(this)),
            obj = ($(this).attr('bfa-obj')?$(this).attr('bfa-obj'):false),
            func = ($(this).attr('bfa-func')?$(this).attr('bfa-func'):false),
            callback = ($(this).attr('bfa-callback')?$(this).attr('bfa-callback'):'reload');

        bfajax(obj, func, inputArr, callback);
    });

    $('table.overview td.editcol button').click(function() {
        var func = $(this).data('func'),
            title = $(this).parents('table').attr('id'),
            vars = {};

        if ( func == 'edit' || func == 'delete' || func == 'activate' ) {
            var idLabel = title + '_id';
            vars[idLabel] = $(this).parents('tr').data('id');
        }

        if ( func == 'install' || func == 'activate' ) {
            vars[func+'Var'] = $(this).parents('tr').children('td[data-id="'+$(this).data(func)+'"]').children('p').html();
        }

        if ( func == 'delete' || func == 'activate' ) {
            var check = ( function(conf) {
                    if (!conf) {
                        close_bflb();
                        return false;
                    } else {
                        bfajax( title, func+'_'+title, vars, 'reload');
                    }
                });

            bfconf(func+' '+title+'?', 'Are you sure you wanna '+func+' the '+title+' with id: '+vars[idLabel]+'?', check);

            //bfajax( title, func+'_'+title, vars, true);
        } else {
            if ( func == 'edit' || func == 'add' || func == 'install' || func == 'activate' ) {
                callback = (function (data) {
                    bflb( func+' '+title, data );
                });
                // callback = (function (data) {
                //     //console.log(data);
                //     bflb('Edit', data);
                // });
            }

            bfajax( title, func+'_'+title, vars, callback);
        }
    });

    $('table.overview tr[data-url]').click(function(e) {
        if ( !$(e.target).is('button[data-func]') && $(e.target).parents('button[data-func]').length == 0 ) {
            e.preventDefault();
            window.location.href = $(e.target).parents('tr[data-url]').data('url');
        }
    });


} )( jQuery );


function set_menu_classes( viewWidth ) {
    /*if (viewWidth < 992) {
        $('nav.navbar').addClass('admin-navbar');
    } else {
        $('nav.navbar.admin-navbar').removeClass('admin-navbar');
    }*/
}

function set_table_labels() {
    /*$('table').each(function() {
        $(this).find('th').each(function() {
            var id = $(this).data('id'),
                val = $(this).html();

            if ( val.length > 0 ) {
                $('td[data-id="'+id+'"]').prepend('<label class="col-lbl">'+val+'</label>');
            }
        });
    });*/
}

function bfajax( object, functionName, functionVars, returnResp ) {
    var object = ( !object ? null : object ),
        functionName = ( !functionName ? null : functionName ),
        functionVars = ( !functionVars ? null : functionVars ),
        returnResp = ( !returnResp ? true : returnResp ),
        baseAjaxUrl = 'http://localhost/bare_fw2/ajax/';

    if ( object !== null ) {
        baseAjaxUrl += object + '/';
    }

    if ( functionName !== null ) {
        baseAjaxUrl += functionName + '/';
    }

    if ( functionVars !== null ) {
        var count = Object.keys(functionVars).length;
            i = 1;
        $.each(functionVars, function( key, val ) {
            baseAjaxUrl += key + '=' + val;
            if (i < count) baseAjaxUrl += '&';
            i++;
        });
    }
    console.log(baseAjaxUrl);
    $.post({
        url: baseAjaxUrl,
    }).done( function( response ) {
            if ( returnResp == true ) {
                return response;
            } else if ( typeof returnResp == 'function' ) {
                returnResp( response );
            } else if ( typeof returnResp == 'string' ) {
                if (check_if_func(returnResp) !== false) {
                    fn = window[returnResp];
                    fn(response);
                } else if ( returnResp == 'log' ) {
                    console.log(response);
                } else {
                    $(returnResp).html(response);
                }
            } else {
                return response
            }
        }
    );

    return false;
}

function check_if_func(returnResp) {
    var fn = window[returnResp];
    if (typeof fn === "function") return true;
    else return false;
}

function gen_input_array( formEl ) {
    var inputArr = {};

    formEl.find( 'input:not([type="radio"],[type="submit"]), input[type="radio"]:checked, textarea, select' ).each( function() {

        var inputName = $( this ).attr( 'name' ),
            inputVal = $( this ).val(),
            subStrPos = inputName.indexOf( '[]' );

        if ( subStrPos > -1 ) {
            var multiInputName = inputName.substring( 0, subStrPos );

            if ( typeof inputArr[ multiInputName ] === 'undefined' ) {
                inputArr[ multiInputName ] = [ inputVal ];
            } else {
                inputArr[ multiInputName ].push( inputVal );
            }

        } else {
            inputArr[ inputName ] = inputVal;
        }


    });
    return inputArr;
}

function bflb( title, content, size, onReload ) {
    var title = (!title ? null: title),
        size = (!size ? 'medium' : size),
        onReload = (!onReload ? 'not-active' : onReload),
        lbContainer = $( 'div.lb-container' ),
        ctContainer = lbContainer.children( 'div.lb-ct-container' ),
        lbContent = ctContainer.children( 'div.lb-ct' ),
        lbTitle = lbContent.children( 'h2.lb-title' ),
        ctElement = lbContent.children( 'div.ct' );

    if ( content.length > 0 ) {

        ctElement.html( content );

        if ( title !== null ) lbTitle.html( title );

        if ( lbContainer.hasClass( 'active' ) === false ) {
            lbContainer.css( 'display', 'block' );
            ctContainer.css( 'display', 'block' );

            if ( size !== null ) {
                switch( size ) {
                    case 'big':
                    case 'container':
                        ctContainer.addClass( 'container' );
                        break;
                    case 'medium':
                        ctContainer.addClass( 'medium' );
                        break;
                    case 'small':
                        ctContainer.addClass( 'small' );
                        break;
                }
            }

            setTimeout( function() {
                lbContainer.addClass( 'active' );
                ctContainer.addClass( 'active' );
            }, 10 );
        }

    } else {
        console.log( 'No content given for lightbox.' );
    }

}

function bfconf( title, message, callback, type ) {
    var title = (!title ? false : title),
        message = (!message ? 'Do you confim?' : message),
        callback = (!callback ? (function(s){console.log(s)}) : callback),
        type = (!type ? 0 : type);

    switch (type) {
        case 0 :
            choice = {'yes': true,'no': false};
            break;
        case 1 :
            choice = {'no': false};
            break;
        case 2 :
            choice = {'yes': true};
            break;
    }

    var confHtml = message+'<br>';

    $.each(choice, function( key, val ) {
        confHtml += '<button class="bfconf-button" data-val="'+val+'">'+key+'</button>';
    });

    bflb(title, confHtml);

    $('button.bfconf-button').click(function(){
        callback($(this).data('val'));
    });

}

function close_bflb() {
    var lbContainer = $( 'div.lb-container' ),
        ctContainer = lbContainer.children( 'div.lb-ct-container' ),
        lbContent = ctContainer.children( 'div.lb-ct' ),
        lbTitle = lbContent.children( 'h2.lb-title' ),
        ctElement = lbContent.children( 'div.ct' );

    if ( lbContainer.hasClass( 'active' ) === true && ctContainer.hasClass( 'active' ) === true ) {
        ctContainer.removeClass( 'active' );

        setTimeout( function() {
            lbContainer.removeClass( 'active' );
        }, 250 );

        setTimeout( function() {
            lbContainer.removeClass( 'active' ).removeAttr( 'style' );
            ctContainer.attr( 'class', 'lb-ct-container' ).removeAttr( 'style' );
            lbTitle.html( '' );
            ctElement.html( '' );
        }, 700 );
    }
}

function reload() {
    location.reload();
}