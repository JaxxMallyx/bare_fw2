<?php

if ( isset($_GET['file_list']) ) include 'file_list.php';

$time_start = microtime(true);
include 'autoloader.php';

$app = new base\controllers\ApplicationController(__DIR__);

$time_end = microtime(true);
$execution_time = ($time_end - $time_start)/60;

if ( \base\controllers\ApplicationController::STATE == 'Development' ) echo '<b>Total Execution Time:</b> '.$execution_time.' Mins';