<?php

namespace base\controllers;
use base\models;

class ApplicationController extends \base\models\ApplicationModel {

    /*
     * public constructor method
     * @param string $baseDir
     */
    public function __construct( string $baseDir )
    {
        include 'config.php';

        if ( !defined('BASEDIR' ) ) define( 'BASEDIR', $baseDir);

        session_start();

        $this->set_dbSettings( $dbsettings );
        $this->set_websiteSettings( $websitesettings );

        $this->set_routObj();

        $this->set_userObj();

        $this->set_pageObj();

        //  $this->appRouter->go_to('example', array('id' => 1, 'function' => 'test'));
    }

    /*
     * protected set_props_by_arr
     * @return void
     */
    protected function set_websiteSettings( $websiteSettings )
    {
        $this->set_siteUrl( $websiteSettings['site_url'] );
        $this->set_siteTitle( $websiteSettings['site_title'] );
        $this->set_siteSubTitle( $websiteSettings['site_subtitle'] );
        $this->set_siteDescription( $websiteSettings['site_description'] );
    }

    /*
     * public static get_db method
     * @return object
     */
    public static function get_db()
    {
        return new \base\lib\Database();
    }

    /*
     * public static go_to method
     * @return object
     */
    public static function go_to( string $routeName, $vars = array() )
    {
        include 'config.php';
        $routeObj = new \base\controllers\RouteController( $websitesettings['site_url'], true );
        $routeObj->go_to( $routeName, $vars );
    }

    /*
     * public static load_file method
     * @param string $fileStr, string $dir, string $subDir, boolean $return
     * @return mixed
     */
    public static function load_file( string $fileStr, string $dir = '', $subDir = '', $return = false )
    {
        $fileDir = BASEDIR . '\\';
        if (!empty($dir)) $fileDir .= $dir;
        if (!empty($subDir)) $fileDir .= '\\' . $subDir;

        $file = $fileDir . '\\' . $fileStr;

        if ( file_exists( $file ) ) {
            if ( $return ) return $file;
            else include $file;
        } else {
            return false;
        }
    }



}