<?php

namespace base\controllers;

use base\models;

class OverviewController extends \base\models\OverviewModel {

    public function __construct( array $overviewArr, $columnRename = array(), string $title = '', string $idCol = '', array $options = array() )
    {
        $this->set_overviewArr( $overviewArr );
        $this->set_head_by_arr();
        if ( !empty( $columnRename ) ) $this->format_columns( $columnRename );
        if ( !empty( $title ) ) $this->set_overviewTitle( $title );
        if ( !empty( $idCol ) ) $this->set_idColumn( $idCol );
        if ( !empty( $options ) ) $this->set_options( $options );
    }

    private function set_head_by_arr()
    {
        $overviewKeys = array_keys( $this->overviewArr[0] );
        $overviewHead = array();
        foreach ( $overviewKeys as $key ) {
            $overviewHead[$key] = ucfirst( strtolower( str_replace( '_', ' ', $key ) ) );
        }
        $this->set_overviewHead( $overviewHead );
    }

    private function format_columns( $columnRename )
    {
        $overviewHead = $this->overviewHead;
        foreach ( $columnRename as $key => $newKey ) {
            if ( $newKey == null ) {
                unset( $overviewHead[$key] );
            } else {
                $overviewHead[$key] = ucfirst( strtolower( $newKey ) );
            }
        }
        $this->overviewHead = $overviewHead;
    }

    public function generate_overview()
    {
        $overviewHtml = '<table id="' . $this->overviewTitle . '" class="overview">';
        $overviewHtml .= '<thead>[overviewHead]</thead>';
        $overviewHtml .= '<tbody>[overviewRows]</tbody>';
        $overviewHtml .= '[overviewFoot]';
        $overviewHtml .= '</table>';


        $headHtml = $this->generate_head();
        $overviewHtml = str_replace( '[overviewHead]', $headHtml, $overviewHtml );

        $rowHtml = $this->generate_rows();
        $overviewHtml = str_replace( '[overviewRows]', $rowHtml, $overviewHtml );

        $footHtml = $this->generate_editColumns( 'foot' );
        $overviewHtml = str_replace( '[overviewFoot]', $footHtml, $overviewHtml );

        return $overviewHtml;
    }

    private function generate_head()
    {
        $headHtml = '<tr>';

        foreach( $this->overviewHead as $key => $title ) {
            $headHtml .= '<th data-id="' . $key . '">' . $title . '</th>';
        }
        $headHtml .= $this->generate_editColumns( 'head' );
        $headHtml .= '</tr>';

        return $headHtml;
    }

    private function generate_rows()
    {
        $rowHtml = '';
        foreach( $this->overviewArr as $rowArr ) {

            $rowAttrs = '';

            if ( filter_var($this->options['idurl'], FILTER_VALIDATE_URL) ) $rowAttrs = 'data-url="' . $this->options['idurl'] . '/' . $rowArr[$this->idColumn] . '" ';
            else throw new \Exception('idurl option needs to be valid url');

            if ( $this->idColumn) $rowHtml .= '<tr data-id="' . $rowArr[$this->idColumn] . '" ' . $rowAttrs .'>';
            else $rowHtml .= '<tr>';

            foreach ($rowArr as $key => $value) {
                if (isset($this->overviewHead[$key])) $rowHtml .= '<td data-id="' . $key . '"><label class="col-lbl">' . ucfirst($key) . '</label><p>' . $value . '</p></td>';
            }

            if ( isset( $this->options ) ) $rowHtml .= $this->generate_editColumns( 'rows' );

            $rowHtml .= '</tr>';
        }
        return $rowHtml;
    }

    private function generate_editColumns( string $pos = 'head' )
    {
        $editColHtml = '';

        if (isset($this->options)) {
            if ($pos == 'rows') {
                $editColHtml .= '<td class="editcol">';
                if ( isset( $this->options['edit']) && $this->options['edit'] == true) {
                    $editColHtml .= '<button data-func="edit" class="col4"><i class="fa fa-edit"></i></button>';
                }
                if ( isset( $this->options['install']) && !empty($this->options['install'])) {
                    $funcCol = ( strlen($this->options['install']) > 1  ? 'data-install="' . $this->options['install'] . '"' : '');
                    $editColHtml .= '<button data-func="install" ' . $funcCol . ' class="success"><i class="fa fa-download"></i></button>';
                }
                if ( isset( $this->options['activate']) && !empty($this->options['activate'])) {
                    $funcCol = ( strlen($this->options['activate']) > 1  ? 'data-activate="' . $this->options['activate'] . '"' : '');
                    $editColHtml .= '<button data-func="activate" ' . $funcCol . ' class="warning"><i class="fa fa-power-off"></i></button>';
                }
                if ( isset( $this->options['delete']) && $this->options['delete'] == true) {
                    $editColHtml .= '<button data-func="delete" class="danger"><i class="fa fa-trash"></i></button>';
                }
                $editColHtml .= '</td>';
            } elseif ($pos == 'head') {
                $editColHtml .= '<th class="editcol"></th>';
            } elseif ($pos == 'foot') {
                if ( isset( $this->options['add']) && $this->options['add'] == true) {
                    $editColHtml .= '<tfoot><tr><td class="editcol">';
                    $buttonStr = 'Add ' . str_replace( '_', ' ', strtolower( $this->overviewTitle ) );

                    $editColHtml .= '<button data-func="add" class="success" id="add_' . strtolower( $this->overviewTitle ) . '"><i class="fa fa-plus"></i></button>';
                    $editColHtml .= '<label class="btnlabel" for="add_' . strtolower( $this->overviewTitle ) . '">' . $buttonStr . '</label>';

                    $editColHtml .= '</td></tr></tfoot>';
                }
            }
        }

        return $editColHtml;
    }
}