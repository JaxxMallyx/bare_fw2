<?php

namespace base\controllers;

use base\models;

class FormController extends \base\models\FormModel {

    public $sanitize;

    /*
     * public constructor method
     * @param string $name, string $method, array $options
     */
    public function __construct( $name = '', $method = 'post', $options = array(), string $sanitize = 'sql' )
    {
        $this->sanitize = $sanitize;
        $this->generate_formElement( $name, $method, $options );
        if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) $this->post_rawValues( $_POST );
    }
    /*
     * public generate_form method
     * returns string
     */
    public function generate_form()
    {
        return str_replace('[input-elements]', $this->format_input_elements(), $this->formElement);
    }

    /*
     * private format_input_elements method
     * @return string
     */
    private function format_input_elements()
    {
        $returnStr = '';

        foreach ( $this->inputArr as $key => $element ) {

            if ( is_string( $element ) ) {
                $returnStr .= $element;
            } else if ( isset($element['elementStr'] ) ) {
                $elementStr = $element['elementStr'];

                unset( $element['elementStr'] );
                $returnStr .= str_replace( '[input-elements]', implode( $element ), $elementStr );
            }

        }

        return $returnStr;
    }

    /*
     * private set_formElement method
     * @param string $name, string $method, array $options
     */
    private function generate_formElement( string $name, $method = 'post', $options = array() )
    {
        $formStr = '<form name="[name]" id="form-[name]" method="[method]" class="[classes]" [ajax-attributes] [attributes]>';
        $formStr .= '[input-elements]';
        $formStr .= '</form>';

        $formStr = str_replace('[method]', $method, $formStr);

        $classesArr = (isset($options['classes']) ? $options['classes'] : false);
        $attributesArr = (isset($options['attributes']) ? $options['attributes'] : false);

        $formStr = $this->set_standard_options( $formStr, $name, $classesArr, $attributesArr );

        $ajaxAttribStr = (isset($options['ajax']) ? $this->generate_ajax_attributes( $options['ajax'] ) : false);

        $formStr = ( !empty($ajaxAttribStr) && $ajaxAttribStr != false ? str_replace( '[ajax-attributes]', $ajaxAttribStr, $formStr ) : str_replace( '[ajax-attributes]', '', $formStr ) );

        $this->set_formElement( $formStr );
    }

    private function generate_ajax_attributes( array $ajaxArr = array() )
    {
        if ( !empty( $ajaxArr ) ) {
            $attribStr = '';
            $attribStr .= ( isset( $ajaxArr['obj'] ) ? 'bfa-obj="' . $ajaxArr['obj'] .'" ' : '' );
            $attribStr .= ( isset( $ajaxArr['func'] ) ? 'bfa-func="' . $ajaxArr['func'] . '" ' : '' );
            $attribStr .= ( isset( $ajaxArr['callback'] ) ? 'bfa-callback="' . $ajaxArr['callback'] . '" ' : '' );
            return $attribStr;
        } else {
            return false;
        }
    }

    /*
     * private set_standard_options method
     * @param string $str, string $name, mixed $classes, mixed $attributes
     * @return string
     */
    private function set_standard_options( string $str, string $name, $classes, $attributes )
    {

        $returnStr = $str;

        if ( $classes ) {
            $classesStr = ( is_array( $classes ) ? implode( ' ', $classes ) : $classes );
            $returnStr = str_replace( '[classes]', $classesStr, $returnStr );
        } else {
            $returnStr = str_replace( ' class="[classes]"', '', $returnStr );
            $returnStr = str_replace( '[classes]', '', $returnStr );
        }

        if ( $attributes ) {
            $attributeStr = $this->generate_attribute_string( $attributes );
            $returnStr =  str_replace( ' [attributes]', ' ' . $attributeStr, $returnStr );
        } else {
            $returnStr = str_replace( ' [attributes]', '', $returnStr );
        }

        $returnStr = str_replace( '[name]', $name, $returnStr );
        $returnStr = str_replace( '[Name]', ucfirst( $name ), $returnStr );

        return $returnStr;
    }

    /*
     * public set_input method
     * @param string $type, string $name, array $options, string $group
     *
     * Types that are to be used:
     * submit, button, taggable, checkbox, radio, text, password, number
     *
     * Options array keys that are used:
     * subtype      : Used for button type input, as type of button.
     * classes      : Used for css classes, can be added as array or string.
     * placeholder  : Used for placeholder attributes on input elements.
     * attributes   : Used for added attributes to input elements. An associative array can be used.
     *                The array keys will be used as attribute key and the array values will be used as the attribute value (if there is any).
     * required     : Can be true or false, and will be set accordingly.
     */
    public function set_input( string $type, string $name, $options = array(), $group = null)
    {
        if ( $this->get_rawValues( $name ) ) $this->sanitize_rawValue( $type, $name );

        $labelExArr = array( 'hidden', 'button', 'submit', 'radio', 'radio-tabs', 'checkbox' );
        $choiceArr = array( 'radio', 'radio-tabs', 'checkbox' );
        $inputStr = ( !in_array( $type, $labelExArr ) ? '<label for="[name]" class="[classes]">[label]' : '' );
        $inputStr .= ( isset( $options['required'] ) ? '<sup>*</sup>' : '' );

        if ( $type == 'radio-tabs' ) $inputStr = '<label class="radio-label [classes]" for="[name]-[value]">[label]';

        if ( isset( $options['label-classes'] ) ) {

            if ( is_array( $options['label-classes'] ) ) $inputStr = str_replace( '[classes]', implode( ' ', $options['label-classes'] ), $inputStr );
            else $inputStr = str_replace( '[classes]', $options['label-classes'], $inputStr );

        }

        switch( $type ) {
            case 'submit':
                $inputStr .= '<input type="submit" name="[name]" id="[name]" class="button [classes]" placeholder="[placeholder]" value="[Name]" [attributes] required />';
                break;
            case 'button':
                $inputStr .= '<button type="[type]" name="[name]" id="[name]" class="button [classes]" [attributes]>[value]</button>';
                $inputStr = ( isset( $options['subtype'] ) ? str_replace( '[type]', $options['subtype'], $inputStr ) : str_replace( '[type]', 'submit', $inputStr ) );
                break;
            case 'taggable':
                $inputStr .= '<input type="text" name="[name]" id="[name]" class="taggable [classes]" placeholder="[placeholder]" value="[value]" autocomplete="off" [attributes] required />';
                $inputStr .= '<div class="tag-field" id="[name]-field" data-for="[name]"><span class="value"></span></div>';
                //$inputStr = '<textarea name="[name]" id="[name]" class="taggable [classes]" placeholder="[placeholder]" autocomplete="off" [attributes] required>[value]</textarea>';
                break;
            case 'textarea':
                $inputStr .= '<textarea name="[name]" class="[classes]" placeholder="[placeholder]" [attributes] required>[value]</textarea>';
                break;
            case 'checkbox':
            case 'radio':
                $inputStr .= '<input type="[type]" name="[name]" id="[name]" class="[classes]" placeholder="[placeholder]" value="[value]" [attributes] selected required />';
                break;
            case 'radio-tabs':
                $inputStr .= '<input type="radio" name="[name]" id="[name]" class="[classes]" placeholder="[placeholder]" value="[value]" [attributes] selected required />';
                break;
            case 'select':
                $inputStr .= '<select name="[name]" id="[name]" class="[classes]" [attributes] required>';
                $inputStr .= $this->generate_options( $options );
                $inputStr .= '</select>';
                unset($options['values']);
                break;
            case 'text':
            case 'password':
            case 'number':
            case 'email':
            case 'url':
            case 'tel':
            case 'hidden':
                $inputStr .= '<input type="[type]" name="[name]" id="[name]" class="[classes]" placeholder="[placeholder]" value="[value]" [attributes] required />';
                break;
            default:
                throw new Exception( 'Type needs to be set. [text, textarea, password, number, submit, button, taggable, checkbox, radio]' );
                break;
        }

        $inputStr .= ( !in_array( $type, $labelExArr ) || $type == 'radio-tabs' ? '</label>' : '' );

        $inputStr = ( strpos( $inputStr, '[type]' ) ? str_replace( '[type]', $type, $inputStr ) : $inputStr );

        if ( isset( $options['placeholder'] ) ) {
            $inputStr =  str_replace( ' placeholder="[placeholder]"', $options['placeholder'], $inputStr );
        } else {
            $inputStr = str_replace( ' placeholder="[placeholder]"', '', $inputStr );
        }

        if ( !isset( $options['required'] ) ) {
            $inputStr = str_replace( ' required', '', $inputStr );
        }

        if ( isset( $options['value'] ) && !in_array( $type, $choiceArr ) ) {
            $inputStr = str_replace( '[value]', $options['value'], $inputStr );
        }
        if ( isset( $options['values'] ) || in_array( $type, $choiceArr ) ) {
            $selected = ( isset( $options['selected'] ) ? $options['selected'] : false );
            $attributeArr = ( isset( $options['attributes'] ) ? $options['attributes'] : false );

            if ( $type == 'radio-tabs' && !$attributeArr ) $attributeArr = array('data-target'=>'[name]-[value]-tab');

            $inputStr = $this->generate_input_by_value( $type, $inputStr, $options['values'], $selected, $attributeArr );
        } else if ( strpos( $inputStr, 'value="[value]"' ) !== false ) {
            $inputStr = str_replace( ' value="[value]"', '', $inputStr );
        } else {
            $inputStr = str_replace( '[value]', '', $inputStr );
        }

        $inputStr = ( isset( $options['label'] ) ? str_replace( '[label]', $options['label'], $inputStr ) : str_replace( '[label]', '[Name]', $inputStr ) );

        $classesArr = ( isset( $options['classes'] ) ? $options['classes'] : false );
        $attributeArr = ( isset( $options['attributes'] ) ? $options['attributes'] : false );

        $inputStr = $this->set_standard_options( $inputStr, $name, $classesArr, $attributeArr );

        $this->set_inputArr( $name, $inputStr, $group );

    }

    /*
     * public set_form_group method
     *
     */
    public function set_form_group($title, $options = array())
    {
        $groupStr = '<div id="[name]" class="form-group [classes]" [attributes]>[input-elements]</div>';

        $classesArr = (isset($options['classes']) ? $options['classes'] : false);
        $attributeArr = (isset($options['attributes']) ? $options['attributes'] : false);

        $groupStr = $this->set_standard_options($groupStr, $title, $classesArr, $attributeArr);

        $this->set_inputArr($title, array('elementStr' => $groupStr) );
    }

    /*
     * private function generate_options
     * @param array $options
     * @return string
     */
    private function generate_options( array $options )
    {
        if ( isset( $options['values'] ) ) {

            $selected = ( isset( $options['selected'] ) ? $options['selected'] : false );
            $returnStr = '';


            foreach ( $options['values'] as $key => $value ) {

                $optionStr = '<option value="' . $key . '" [attributes] selected>[label]</option>';

                if ( $key != $selected ) $optionStr = str_replace( 'selected', '', $optionStr );
                if ( isset( $options['op-attributes'] ) ) $optionStr = str_replace( '[attributes]', $this->generate_attribute_string( $options['op-attributes'] ), $optionStr );

                $optionStr = str_replace('[label]', $value, $optionStr);
                $returnStr .= $optionStr;
            }

            return $returnStr;

        } else {

            throw new Exception('Values element of the options array needs to be set for this input type.');

        }
    }

    /*
     * private generate_input_by_value method
     * @param string $type, string $inputStr, array $values, mixed $selected, array $attributes
     * @return string
     */
    private function generate_input_by_value( string $type, string $inputStr, array $values, $selected = null, $attributes = array() )
    {
        if ( $values ) {

            $returnStr = '';

            if ( $type == 'radio-tabs' ) $returnStr .= '<div class="radio-tabs">';

            foreach ( $values as $value ) {

                if ( $attributes) $valueInputStr = str_replace( '[attributes]', $this->generate_attribute_string( $attributes ), $inputStr );
                else $valueInputStr = $inputStr;

                if ( !is_array( $value ) ) {

                    $valueInputStr = str_replace( '[value]', $value, $valueInputStr );
                    $valueInputStr = str_replace( 'id="[name]"', 'id="[name]-' . $value . '"', $valueInputStr );

                } else {

                    if ( isset( $value['value'] ) ) $valueInputStr = str_replace( '[value]', $value['value'], $valueInputStr );
                    else throw new Exception( 'Value key needs to be set in value array.' );

                    if ( isset( $value['label'] ) ) $valueInputStr = str_replace( '[label]', ucfirst( strtolower( $value['label'] ) ), $valueInputStr );
                    else throw new Exception( 'Label key needs to be set in value array.' );

                }

                if ( strpos( $valueInputStr, '[label]' ) > -1 ) $valueInputStr = str_replace( '[label]', $value, $valueInputStr );

                if ( $selected && $selected != $value ) $valueInputStr = str_replace( 'selected', '', $valueInputStr );
                else if ( $type != 'radio-tabs' || $values[0] != $value ) $valueInputStr = str_replace( 'selected', '', $valueInputStr );

                $returnStr .= $valueInputStr;
            }

            if ( $type == 'radio-tabs' ) $returnStr .= '</div>';

            return $returnStr;

        } else if ( $type == 'radio' || $type == 'checkbox' || $type == 'radio-tabs' ) {

            throw new Exception('Values element of the options array needs to be set for this input type.');

        } else {

            return $inputStr;

        }
    }

    /*
     * private generate_attribute_string method
     * @param array, string $attributes
     * @return string
     */
    private function generate_attribute_string( $attributes )
    {
        if ( is_array( $attributes ) ) {

            $returnStr = '';

            foreach ( $attributes as $key => $value ) {
                $returnStr .= $key . '="' . $value . '" ';
            }

            return $returnStr;

        } else if ( is_string( $attributes ) ) {

            return $attributes;

        } else {

            throw new Exception('Attributes needs to be either string or array [key=>value]');

        }
    }

    /*
     * private sanitize_values method
     * @param array $postArr
     */
    private function post_rawValues( array $postArr )
    {
        foreach ( $postArr as $key => $value ) {
            $this->set_rawValues( $key, $value );
        }
    }

    /*
     * private sanitize_rawValues method
     * @param string $type, string $name
     */
    private function sanitize_rawValue( string $type, string $name )
    {
        $sanitize = new \base\lib\Sanitize();

        if ( $type != 'number' ) {
            if ( $this->sanitize == 'sql' ) $formValue = $sanitize->input( $this->get_rawValues( $name ), array( 'SQL', 'UTF8' ) );
            elseif ( $this->sanitize == 'ldap' ) $formValue = $sanitize->input( $this->get_rawValues( $name ), array( 'LDAP', 'UTF8' ) );
            elseif ( $this->sanitize == 'html' ) $formValue = $sanitize->input( $this->get_rawValues( $name ), array( 'HTML', 'UTF8' ) );
            elseif ( $this->sanitize == 'paranoid' ) $formValue = $sanitize->input( $this->get_rawValues( $name ), array( 'PARANOID', 'UTF8' ) );
            elseif ( $this->sanitize == 'system' ) $formValue = $sanitize->input( $this->get_rawValues( $name ), array( 'SYSTEM', 'UTF8' ) );
        } else {
            $formValue = $sanitize->input( $this->get_rawValues( $name ), array( 'FLOAT', 'UTF8' ) );
        }

        $this->set_formValues( $name, $formValue );
    }
}