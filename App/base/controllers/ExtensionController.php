<?php

namespace base\controllers;
use base\models;

class ExtensionController extends \base\models\ExtensionModel {

    public function __construct( int $id = 0, string $title = '', string $dir = '', $description = '', string $version = '0.0.0', $db = false, $active = false, string $mainFile = '' )
    {
        if ( !empty($id) ) $this->set_id( $id );
        if ( !empty($title) ) $this->set_title( $title );
        if ( !empty($dir) ) $this->set_dir( $dir );
        if ( !empty($description) ) $this->set_description( $description );

        $this->set_version( $version );

        $this->set_db( $db );
        $this->set_active( $active );

        if ( !empty($mainFile) ) $this->set_mainFile( $mainFile );
        else $this->get_mainfile_by_dir();

        $this->set_extObj_by_mainfile();
    }

    /*
     * public static new_ext_by_dir method
     * @param string $dir
     * @return array
     */
    public static function new_ext_by_dir( string $dir )
    {
        $mainfile = \base\controllers\ApplicationController::load_file('extmain.php', 'App\ext', $dir, true);

        if ( file_exists( $mainfile ) ) {
            $file = file_get_contents( $mainfile );

            $docComments = array_filter( token_get_all($file), function($entry) { return $entry[0] == T_COMMENT; });
            $docComments = array_shift( $docComments )[1];

            foreach ( explode('*', $docComments) as $commentLine ) {
                $lowComment = strtolower( $commentLine );
                if ( $commentLine != '/' && !empty($commentLine) ) {
                    if ( strpos( $lowComment, 'title' ) != false ) $title = str_replace( 'Title: ', '', trim( preg_replace('/\s\s+/', ' ', $commentLine ) ) );
                    if ( strpos( $lowComment, 'version' ) != false ) $version = str_replace( 'Version: ', '', trim( preg_replace('/\s\s+/', ' ', $commentLine ) ) );
                    if ( strpos( $lowComment, 'description' ) != false ) $description = str_replace( 'Description: ', '', trim( preg_replace('/\s\s+/', ' ', $commentLine ) ) );
                }
            }

            if ( $title && $version ) {
                $description = ( isset($description) ? $description : '');
                $extNew = new \base\controllers\ExtensionController( 0, $title, $dir, $description, $version, false, false, $mainfile );

                return $extNew;

            } else {
                throw new \Exception( 'Title and version should be specified in extmain.php top comments.' );
            }
        }
    }

    /*
     * public static get_ext_by
     * @param array $extArr, string $key, mixed $value
     * @return object
     */
    public static function get_ext_by( array $extArr, string $key = 'dir', $value )
    {
        foreach ($extArr as $extension) {
            if ($extension->$key == $value) return $extension;
        }
        return false;
    }

    private function get_mainfile_by_dir()
    {
        if ( $this->dir && $mainfile = \base\controllers\ApplicationController::load_file('extmain.php', 'App\ext', $this->dir, true) ) {
            $this->set_mainFile( $mainfile );
        }
    }

    private function set_extObj_by_mainfile()
    {
        if ( $this->mainFile ) {
            include $this->mainFile;
            $objStr = '\ext\\' . $this->dir . '\\' . $this->dir;
            $obj = new $objStr();
            $this->extObj = $obj;
        }
    }

}