<?php

namespace base\controllers;

use base\models;

class MenuController extends \base\models\MenuModel
{
    /*
     * public constructor method
     * @return void
     */
    public function __construct( $menuId = null, $menuTitle = '' )
    {
        if ( $menuId ) $this->get_menu( (int)$menuId, 'id' );
        if ( $menuTitle ) $this->get_menu( (string)$menuTitle, 'title' );
    }

    /*
     * private get_menu method
     * @return void, boolean
     */
    public function get_menu( $var, $type = 'title' )
    {
        if ( $type == 'title' ) $whereStr = 'menu_title = :title';
        elseif ( $type == 'id' ) $whereStr = 'menu_id = :id';

        $menuSql = 'SELECT * FROM menu WHERE ' . $whereStr;

        $db = \base\controllers\ApplicationController::get_db();

        $db->query( $menuSql );
        $db->bind( ':' . $type, $var );

        if ( $db->execute() && $db->resultset() ) {
            $menuArr = $db->resultset()[0];
            $this->set_menuId( (int)$menuArr['menu_id'] );
            $this->set_menuTitle( $menuArr['menu_title'] );
            $this->get_menuItems_from_db();
        } else {
            return false;
        }
    }

    /*
     * private get_menuItems method
     * @return void
     */
    private function get_menuItems_from_db()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query( 'SELECT item_id, parent_id, item_title, item_url FROM menu_items WHERE menu_id = :menuId');
        $db->bind( ':menuId', $this->get_menuId() );

        if ( $db->execute() && $db->resultset() ) {
            $this->set_menuItems( $this->format_menuItems( $db->resultset() ) );
        }
    }

    /*
     * private format_menuItems
     * @return array
     */
    private function format_menuItems( $itemArr )
    {
        $menuItemArr = array();

        foreach ( $itemArr as $item ) {
            $newItem = array(
                'title' => $item['item_title'],
                'url' => $item['item_url']
            );

            if ( isset( $menuItemArr[(int)$item['parent_id']] ) ) {
                $menuItemArr[(int)$item['parent_id']]['subs'][(int)$item['item_id']] = $newItem;
            } else {
                $menuItemArr[(int)$item['item_id']] = $newItem;
            }
        }

        return $menuItemArr;
    }

}