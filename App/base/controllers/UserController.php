<?php

namespace base\controllers;

use base\models;

class UserController extends \base\models\UserModel {

    /*
     * public user constructor method
     * @param mixed $userId, $username, $email, $password, $hash, $firstname, $prefix, $lastname, $roleId
     */
    public function __construct( int $userId = 0, string $username = '', string $email = '', string $password = '', string $firstname = '', string $prefix = '', string $lastname = '', int $roleId = 0 )
    {
        $this->set_userId( $userId );
        $this->set_username( $username );
        $this->set_email( $email );
        $this->set_password( $password );
        $this->set_firstname( $firstname );
        $this->set_prefix( $prefix );
        $this->set_lastname( $lastname );
        $this->set_roleId( $roleId );
    }

    public function set_user_by_session()
    {
        if ( isset( $_SESSION ) && !empty( $_SESSION ) ) {
            foreach ($_SESSION as $key => $value) {
                if (strpos($key, 'bfUser2') == 0) {
                    $hash = substr($key, 7);
                    $userId = $value;
                }
            }

            if ( !isset( $hash ) && !isset( $userId ) ) return false; // No bare framework user set.

            return $this->get_user_by( array( 'user_id' => $userId,'hash' => $hash ) );
        } else {
            return false;
        }
    }

    /*
     * public check_session method
     * @return bool
     */
    public static function check_session()
    {
        return self::set_user_by_session();
    }

    /*
     *
     */
    private static function get_user_by_session( $column = false )
    {
        if ( isset( $_SESSION ) && !empty( $_SESSION ) ) {
            foreach ( $_SESSION as $key => $value ) {
                if ( strpos( $key, 'bfUser2' ) == 0 ) {
                    $hash = substr( $key, 7 );
                    $userId = $value;
                } else {
                    return false; // No user session set.
                }
            }
            $db = \base\controllers\ApplicationController::get_db();

            if ( !$column ) {
                $db->query('SELECT user_id FROM user WHERE hash = :hash AND user_id = :userId');
                $db->bind(':hash', $hash);
                $db->bind(':userId', $userId);

                if ($db->execute() && $db->single()) {
                    return true; // Session valid
                } else {
                    return false; // Session invalid
                }
            } else if ( $column != 'all' ) {
                $db->query('SELECT AES_DECRYPT(' . $column . ', "' . AES . '") as ' . $column . ' FROM user WHERE hash = :hash AND user_id = :userId');
                $db->bind(':hash', $hash);
                $db->bind(':userId', $userId);
                if ($db->execute() && $db->single()) {
                    return $db->single()[$column];
                } else {
                    return false; // Session invalid
                }
            } else {

            }

        } else {
            return false; // No session set.
        }
    }

    public function register_user()
    {
        $check = ( $this->check_user_by( 'username', $this->get_username() ) && $this->check_user_by( 'email', $this->get_email() ) ? false : true );

        if ( $check ) {
            $this->generate_hash();
            $this->obscure_password();
            return $this->save_user();
        } else {
            return $check;
        }
    }

    public function login_user()
    {
        $user = $this->get_user_by( array( 'username' => $this->get_username() ), false );

        if ( $user ) {
            $this->set_hash( $user['hash'] );
            $this->snap_hash();

            if ( $this->verify_password( $this->get_password(), $user['password'] ) ) {
                $this->set_user_by_arr( $user );
                $this->set_session();
                return true;
            } else {
                return false;
            }
        }
    }

    public function check_user_by( $key = 'username', $var )
    {
        $user = $this->get_user_by( array( $key => $var ), false );
        if ( $user !== false ) return true; // user exists
        else return false; // user doesn't exist
    }

    public function get_user_by( array $vars, $set = true )
    {
        $db = \base\controllers\ApplicationController::get_db();

        $count = count( $vars );
        $keys = array_keys( $vars );
        $vars = array_values( $vars );

        $whereStr = '';

        for( $i = 0; $i < $count; $i++ ) {
            if ( $keys[$i] != 'hash' && $keys[$i] != 'user_id' ) {
                $whereStr .= $keys[$i] . ' = AES_ENCRYPT( :' . $keys[$i] . ', "' . AES . '" )';
            } else {
                $whereStr .= $keys[$i] . ' = :' . $keys[$i];
            }
            $whereStr .= ( $i + 1 < $count ? ' AND ' : '' );
        }

        $db->query( 'SELECT 
                             user_id, 
                             AES_DECRYPT(email, "'.AES.'") as email,
                             AES_DECRYPT(username, "'.AES.'") as username,
                             AES_DECRYPT(password, "'.AES.'") as password,
                             hash,
                             AES_DECRYPT(firstname, "'.AES.'") as firstname,
                             AES_DECRYPT(prefix, "'.AES.'") as prefix,
                             AES_DECRYPT(lastname, "'.AES.'") as lastname,
                             user.role_id AS role_id,
                             user_role.level AS level
                          FROM user
                          LEFT JOIN user_role USING (role_id)
                          WHERE '.$whereStr );

        for( $i = 0; $i < $count; $i++ ) {
            $db->bind( ':' . $keys[$i], $vars[$i] );
            //print_r('\nVARS:'.$vars[$i]);
        }

        if ( $db->execute() && $db->resultset() ) {
            if ( $set ) {
                $userArr = $db->resultset()[0];
                $this->set_user_by_arr( $userArr );
                return $this;
            } else {
                return $db->resultset()[0];
            }
        } else {
            return false;
        }
    }

    private function generate_hash()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*_=+-?';
        $charactersLength = strlen( $characters );
        $randomString = '';
        for ( $i = 0; $i < 32; $i++ ) {
            $randomString .= $characters[rand( 0, $charactersLength - 1 )];
        }
        $this->set_hash( $randomString );
        $this->snap_hash();
    }

    private function snap_hash()
    {
        $hashArr = array();
        $hashArr[] = substr( $this->get_hash(), 0, 12);
        $hashArr[] = substr( $this->get_hash(), 12, 20);

        $this->set_hashArr( $hashArr );
    }

    private function obscure_password()
    {
        $passwordStr = $this->get_hashArr( 0 ) . $this->get_password() . $this->get_hashArr( 1 );
        $passwordStr = password_hash( $passwordStr, PASSWORD_BCRYPT );

        $this->set_password( $passwordStr );
    }

    private function verify_password( string $formPassStr, string $dbPassStr )
    {
        $formPassStr = $this->get_hashArr( 0 ) . $formPassStr . $this->get_hashArr( 1 );
        return password_verify( $formPassStr, $dbPassStr );
    }

    private function save_user()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $roleId = ( !$this->get_roleId() ? 3 : $this->get_roleId() );

        $db->query('INSERT INTO user (
            username, 
            email, 
            password, 
            hash, 
            firstname, 
            prefix, 
            lastname, 
            role_id
        ) VALUES (
            AES_ENCRYPT(:username, "' . AES . '"),
            AES_ENCRYPT(:email, "' . AES . '"),
            AES_ENCRYPT(:password, "' . AES . '"),
            :hash,
            AES_ENCRYPT(:firstname, "' . AES . '"),
            AES_ENCRYPT(:prefix, "' . AES . '"),
            AES_ENCRYPT(:lastname, "' . AES . '"),
            :role_id
        )');

        $db->bind( ':username', $this->get_username() );
        $db->bind( ':email', $this->get_email() );
        $db->bind( ':password', $this->get_password() );
        $db->bind( ':hash', $this->get_hash() );
        $db->bind( ':firstname', $this->get_firstname() );
        $db->bind( ':prefix', $this->get_prefix() );
        $db->bind( ':lastname', $this->get_lastname() );
        $db->bind( ':role_id', $roleId );

        if ( $db->execute() && $db->lastInsertId() ) {
            $this->set_userId( (int)$db->lastInsertId() );
            return true;
        } else {
            return false;
        }
    }

    private function set_user_by_arr( array $userArr )
    {
        $this->set_userId( (int)$userArr['user_id'] );
        $this->set_username( $userArr['username'] );
        $this->set_email( $userArr['email'] );
        $this->set_hash( $userArr['hash'] );
        $this->snap_hash();
        $this->set_firstname( $userArr['firstname'] );
        $this->set_prefix( $userArr['prefix'] );
        $this->set_lastname( $userArr['lastname'] );
        $this->set_roleId( (int)$userArr['role_id'] );
        $this->set_level( (int)$userArr['level'] );
    }

    private function set_session()
    {
        $_SESSION['bfUser2' . $this->get_hash()] = $this->userId;
    }

    public function get_all_roles()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query( 'SELECT role_id, title FROM user_role');

        if ( $db->execute() && $db->resultset() ) {
            return $db->resultset();
        } else {
            return false;
        }
    }
}