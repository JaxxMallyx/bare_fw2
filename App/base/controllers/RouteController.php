<?php

namespace base\controllers;
use \base\models;

class RouteController extends \base\models\RouteModel {

    /*
     * public RouteController constructor method
     * @return void
     */
    public function __construct( string $siteUrl = '', $static = false )
    {
        $this->set_baseUrl( $siteUrl );

        $this->get_dbRoutes();

        $this->set_requestUrl( $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] );

        $this->parse_url();

    }

    /*
     * private get_dbRoutes method
     * Gets pages from db and uses set_pageRoutes to set them as routes.
     * @return void
     */
    private function get_dbRoutes()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query( 'SELECT url, title, roles FROM page WHERE active = 1' );

        if ( $db->execute() && $db->resultset() ) {
            $this->set_pageRoutes( $db->resultset() );
        }
    }

    /*
     * private set_pageRoutes method
     * @param array $pageArr
     */
    private function set_pageRoutes( array $pageArr )
    {
        foreach( $pageArr as $page ) {
            $auth = ( $page['roles'] !== null ? true : false );
            if ( !isset( $this->routes[$page['url']] ) ) $this->add_route( $page['title'], $page['url'], $auth );
        }
    }

    /*
     * private parse_url method
     * @return void
     */
    private function parse_url()
    {
        $urlVarStr = str_replace( BASEURL, '', $this->requestUrl );
        $urlVars = ( strpos( $urlVarStr, '/' ) !== false ? explode( '/', $urlVarStr ) : array( $urlVarStr ) ) ;
        $count = count( $urlVars );

        $routeVars = array();

        if ( $count > 0 ) {
            for ( $i = 0; $i < $count; $i++ ) {
                $urlVar = $urlVars[ $i ];
                $urlVar = ( $urlVar === '' ? 'home' : $urlVar );
                $urlVar = ( $this->pageUrl == 'ajax' ? $urlVars[$i] : $urlVar );
                
                if ( $i == 0 ) {
                    //var_dump($urlVar);
                    $routeUrl = $this->get_route( $urlVar, 'url' );
                    //var_dump($routeUrl);
                    if ( $routeUrl === false ) $urlVar = '404';
                    //var_dump($urlVar);
                    $this->set_props_by_route( $urlVar );
                    $routeVars = $this->get_route( $urlVar, 'vars' );
                } elseif ( !empty( $urlVar ) ) {
                    $routeVarKey = (int)$i-1;
                    $key = ( !empty( $routeVars ) && !empty(array_keys( $routeVars )[$routeVarKey]) ? array_keys( $routeVars )[$routeVarKey] : false );
                    $type = ( !empty( $routeVars ) && !empty(array_values( $routeVars )[$routeVarKey]) ? array_values( $routeVars )[$routeVarKey] : false );

                    if ( $key && $type ) $this->set_pageVar( $key, $this->parse_var_by_type( $urlVar, $type ) );
                }
            }
        } else {
            $this->set_pageUrl();
        }

        $this->go_to();
    }

    /*
     * public go_to method
     * Used for navigation
     * @return void
     */
    public function go_to( $routeName = '', $vars = array() )
    {
        if ( empty( $routeName ) ) {
            $routeName = parent::get_current_route( 'routename' );
        }
        if ( empty( $vars ) ) {
            $vars = $this->pageVars;
        }

        $urlVarStr = (!empty($vars) ? $this->format_vars( $vars, parent::get_route( $routeName, 'vars' ) ) : '' );
        $urlStr = BASEURL . parent::get_route($routeName, 'url' ) . $urlVarStr;

        if ($urlStr != $this->requestUrl) {
            header('Location: ' . $urlStr);
        }
    }

    /*
     * private format_vars method
     * Used to format and filter variables based on information in the route array.
     * @return string
     */
    private function format_vars( array $vars = array(), array $routeVars )
    {
        if ( isset($routeVars) ) {

            if ( !empty( $vars ) ) {
                $varStr = '';

                foreach ($routeVars as $varKey => $varType) {

                    $required = (strpos($varType, '*') === 0 ? true : false);
                    $type = ($required ? str_replace('*', '', $varType) : $varType);

                    if ($required && $vars[$varKey] || !empty($vars[$varKey])) {
                        $var = $this->parse_var_by_type($vars[$varKey], $type);
                        $varStr .= '/' . $var;
                    }
                }

                return $varStr;
            } else {

                return;

            }

        } else if ( \base\models\ApplicationModel::STATE == 'DEVELOPMENT' ) {

            //throw new \Exception('Route variables array should be set in \base\models\RouteModel. Even if it\'s empty. ');

        }
    }

    /*
     * private parse_var_by_type method
     * Parses page variables by type defined in routes.
     * @param mixed $var, string $type
     * @return mixed
     */
    private function parse_var_by_type( $var, string $type )
    {
        $type = str_replace( '*', '', $type );

        switch ($type) {
            case 'bool':
                $var = (bool)filter_var( $var, FILTER_VALIDATE_BOOLEAN );
                break;
            case 'int' :
                $var = (int)filter_var( $var, FILTER_SANITIZE_NUMBER_INT );
                break;
            case 'float' :
                $var = floatval( filter_var( $var, FILTER_SANITIZE_NUMBER_FLOAT ) );
                break;
            case 'string' :
                $var = filter_var($var, FILTER_SANITIZE_STRING);
                break;
            default :
                if ( \base\models\ApplicationModel::STATE == 'DEVELOPMENT' ) {
                    throw new \Exception('Route variable type should be set in \base\models\RouteModel.');
                } else {
                    $this->go_to( '404' );
                }
                break;
        }

        return $var;
    }

}