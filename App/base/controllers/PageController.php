<?php

namespace base\controllers;

use base\models;

class PageController extends \base\models\PageModel
{

    /*
     * public page constructor method
     * @return void
     */
    public function __construct( $routeObj, $userObj )
    {
        $this->set_routeObj( $routeObj );

        $this->set_userObj( $userObj );

        $this->check_auth();

        //$this->set_dependencies( 'header', 'materialize', 'css', null, 'materialize' );
        $this->set_system_mode_style();

        $this->load_menuObj('default');

        if ( $this->routeObj->pageUrl == 'adminpanel' ) {
            $this->load_specPageObj( 'admin' );
        } elseif ( $this->routeObj->pageUrl == 'ajax' ) {
            $this->load_specPageObj( 'ajax' );
            exit;
        } else {
            $this->load_specPageObj();
        }

        if ( \base\controllers\ApplicationController::STATE == 'DEVELOPMENT' ) $this->compile_sass();

        $this->set_dependencies( 'header', 'fonts', 'css', 'lib/fonts', 'fonts' );
        //$this->set_dependencies( 'header', 'top', 'css', null, 'top-style' );

        $this->set_dependencies( 'footer', 'animate.min', 'css', 'lib', 'animate' );
        $this->set_dependencies( 'footer', 'all.min', 'css', 'lib/fontawesome', 'fontawesome' );
        $this->set_dependencies( 'footer', 'bottom', 'css', null, 'bottom-style' );
        $this->set_dependencies( 'footer', 'jquery-3.3.1.min', 'js', 'lib', 'jquery' );
        $this->set_dependencies( 'footer', 'materialize.min', 'js', 'lib/materialize', 'materialize' );
        $this->set_dependencies( 'footer', 'script', 'js', null, 'script' );

        if ( $this->routeObj->pageUrl != 'ajax' ) {
            $this->get_pageHeader();

            $this->get_pageTemplate();

            $this->get_pageFooter();
        }
    }

    /*
     * private check_auth method
     * @return void
     */
    private function check_auth( $return = false)
    {
        if ( $this->routeObj->get_current_route( 'auth' ) && !$this->userObj ) {
            $this->routeObj->go_to( 'login' );
        }
    }

    /*
     * private load_specPageObj method
     * @param bool $admin
     * @return void
     */
    private function load_specPageObj( $type = false )
    {
        if ( $type == false ) {
            $pageDir = BASEDIR . '\Page\\';
            $urlStr = ucfirst($this->routeObj->pageUrl);
            $titleStr = $urlStr . 'Page';
            $objStr = '\Page\\';
            $this->routeObj->set_pageTitle( $urlStr );
        } elseif ( $type == 'admin' ) {
            $this->check_level( 2, 'logout' );
            $pageDir = BASEDIR . '\App\admin\page\\';
            $urlStr =  ucfirst( ( $this->routeObj->pageVars['page_title'] == '' ? 'dashboard' : $this->routeObj->pageVars['page_title'] ) );
            $titleStr = $urlStr . 'Admin';
            $objStr = '\admin\page\\';

            $this->routeObj->set_pageTitle( ucfirst( $urlStr ) );

            $this->set_system_mode_style( 'admin' );
            $this->menuObj->get_menu( 'admin-menu' );
        } elseif ( $type == 'ajax' ) {
            $pageDir = BASEDIR . '\App\ajax\\';
            $urlStr =  ucfirst( $this->routeObj->pageVars['obj'] );
            $titleStr = $urlStr . 'Ajax';
            $objStr = '\ajax\\';

            $this->routeObj->set_pageTitle( ucfirst( strtolower( $urlStr ) . 'Admin' ) );
        }

        $this->load_pageObj( $pageDir, $urlStr, $titleStr, $objStr );
    }

    private function load_pageObj( string $pageDir, string $urlStr, string $titleStr, string $objStr, $default = 'DefaultPage' )
    {
        if ( file_exists( $pageDir . $urlStr . '.php' ) ) {
            $objStr .= ucfirst( $urlStr );
        } elseif ( file_exists( $pageDir . $titleStr . '.php' ) ) {
            $objStr .= ucfirst( $titleStr );
        } else {
            $objStr .= $default;
        }

        $this->set_defPageObj( new $objStr( $this ) );
    }

    /*
     * private load_menuObj method
     * @param string $title, boolean $return
     * @return void, object
     */
    public function load_menuObj( string $title, $return = false )
    {
        $this->set_menuObj( new \base\controllers\MenuController( null, $title ) );

        if ( $return ) $this->menuObj;
    }

    private function compile_sass()
    {
        $sassDir =  BASEDIR . '\Public\src\style\scss\\';
        $cssDir =  BASEDIR . '\Public\src\style\css\\';

        $scanDir = scandir( $sassDir );

        $scssBase = new \base\lib\Scss();

        $scss = $scssBase->get_compiler();
        $scss->setFormatter('Leafo\ScssPhp\Formatter\Compressed');
        $scss->setImportPaths( $sassDir );

        foreach ( $scanDir as $file ) {
            if ( strpos( $file, '.' ) !== 0 && strpos( $file, '_' ) !== 0 && $file != 'imports' ) {
                $lastMod = ( time() - filemtime( $sassDir . $file ) ) / 60;

                if ( $lastMod < 3 || strpos( $file, 'top' ) !== false ) {
                    $sassFile = file_get_contents($sassDir . $file);
                    $cssFile = $cssDir . str_replace('scss', 'css', $file);
                    $compiled = $scss->compile($sassFile);

                    if (!empty($compiled)) {
                        file_put_contents($cssFile, $compiled);
                    }
                }
            }
        }
    }

    public function set_system_mode_style( $name = null )
    {
        $mode = ( !empty( $name ) ? $name . '_mode' : 'mode' );
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT option_value FROM system_options WHERE option_key = :mode');
        $db->bind( ':mode', $mode );

        if ($db->execute() && $db->single()) {
            $mode = $db->single()['option_value'];
        } else {
            $mode = 'default';
        }

        $this->set_dependencies( 'header', 'materialize.' . $mode, 'css', null, 'materialize' );
        $this->set_dependencies( 'header', 'top.' . $mode, 'css', null, 'top' );
    }

    /*
     * private get_pageHeader method
     * @return void
     */
    private function get_pageHeader()
    {
        $page = $this->get_defPageObj();

        $headerFile = $this->get_templateFile( 'header' );

        require_once $headerFile;
    }

    /*
     * private get_pageTemplate method
     * @return void
     */
    private function get_pageTemplate()
    {
        $page = $this->get_defPageObj();

        $tmplTitle = strtolower( str_replace( ' ', '-', $this->routeObj->pageTitle ) );

        if ( $this->routeObj->pageUrl == 'adminpanel' ) $dir = 'App\admin\views';
        else $dir = 'Public\views';

        $mainFile = \base\controllers\ApplicationController::load_file( 'main.phtml', $dir, $this->routeObj->pageUrl, true );

        if ( !$mainFile ) $mainFile = \base\controllers\ApplicationController::load_file( 'main.phtml', $dir, $tmplTitle, true );
        if ( !$mainFile ) $mainFile = \base\controllers\ApplicationController::load_file( $tmplTitle . '.phtml', $dir, null, true );
        if ( !$mainFile ) $mainFile = \base\controllers\ApplicationController::load_file( 'main.phtml', $dir, 'default', true );

        require_once $mainFile;
    }

    /*
     * private get_pageFooter method
     * @return void
     */
    private function get_pageFooter()
    {
        $page = $this->get_defPageObj();

        $footerFile = $this->get_templateFile( 'footer' );

        require_once $footerFile;
    }

    /*
     * private get_templateFile method
     * @param string $fileName, boolean $default
     * @return string
     */
    private function get_templateFile( string $fileName, $default = true )
    {
        $file = \base\controllers\ApplicationController::load_file( $fileName . '.phtml', 'Public\views', $this->routeObj->pageUrl, true );
        if ( !$file && $default ) $file = \base\controllers\ApplicationController::load_file( $fileName . '.phtml', 'Public\views', 'default', true );

        return $file;
    }

    /*
     * public get_part method
     * @param string $fileName
     */
    public function get_part( string $fileName )
    {
        require_once \base\controllers\ApplicationController::load_file( $fileName . '.phtml', 'Public\views', 'parts', true );
    }

    /*
     * public svg_helper method
     * @param string $fileName
     * @return string
     */
    public function svg_helper( string $fileName )
    {
        $file = BASEDIR . '\Public\img\svg\\' . $fileName . '.svg';
        if ( file_exists( $file ) ) {
            return file_get_contents( $file );
        }
    }

    /*
     * public static get_user_title method
     * @return string
     */
    public function get_user_title()
    {
        if ( !$this->userObj || !$this->userObj->get_firstname() ) {
            return 'User';
        } else {
            return 'Hallo ' . $this->userObj->get_firstname();
        }
    }

    public function check_level( int $authLevel, string $routeName = '', array $routeVars = array() )
    {
        $check = ( $this->userObj != false ? $this->userObj->get_level() <= $authLevel : false );

        if ( !$check && empty( $routeName ) ) {
            if ( $this->routeObj->pageUrl == 'ajax' ) {
                echo '404';
                return false;
            } else {
                \base\controllers\ApplicationController::go_to( 'home' );
            }
        } elseif ( !$check ) {
            \base\controllers\ApplicationController::go_to( $routeName, $routeVars );
        }

        return true;
    }
}