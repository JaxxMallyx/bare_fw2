<?php

namespace base\lib;

///////////////////////////////////////
// sanitize.inc.php
// Sanitization functions for PHP
// by: Gavin Zuchlinski, Jamie Pratt, Hokkaido
// webpage: http://libox.net
// Last modified: September 27, 2003
//
// Many thanks to those on the webappsec list for helping me improve these functions
///////////////////////////////////////
// Function list:
// sanitize_paranoid_string($string) -- input string, returns string stripped of all non
//           alphanumeric
// sanitize_system_string($string) -- input string, returns string stripped of special
//           characters
// sanitize_sql_string($string) -- input string, returns string with slashed out quotes
// sanitize_html_string($string) -- input string, returns string with html replacements
//           for special characters
// sanitize_int($integer) -- input integer, returns ONLY the integer (no extraneous
//           characters
// sanitize_float($float) -- input float, returns ONLY the float (no extraneous
//           characters)
// sanitize($input, $flags) -- input any variable, performs sanitization
//           functions specified in flags. flags can be bitwise
//           combination of PARANOID, SQL, SYSTEM, HTML, INT, FLOAT, LDAP,
//           UTF8
///////////////////////////////////////

class Sanitize
{

    public function __construct()
    {
    }

    /*
     * public sanitize method
     * @param string $input, constant $flags, string $min, string $max
     * @return string
     *
     * glue together all the other functions
     */
    public function input( $input,  $flags, $min = 0, $max = 999999 )
    {
        if ( $flags == 'UTF8' || in_array('UTF8', $flags ) ) {
            $input = $this->my_utf8_decode( $input );
        }
        if ( $flags == 'PARANOID' || in_array('PARANOID', $flags ) ) {
            $input = $this->sanitize_paranoid_string( $input, $min, $max );
        }
        if ( $flags == 'INT' || in_array('INT', $flags ) ) {
            $input = $this->sanitize_int( $input, $min, $max );
        }
        if ( $flags == 'FLOAT' || in_array('FLOAT', $flags ) ) {
            $input = $this->sanitize_float( $input, $min, $max );
        }
        if ( $flags == 'HTML' || in_array('HTML', $flags ) ) {
            $input = $this->sanitize_html_string( $input, $min, $max );
        }
        if ( $flags == 'SQL' || in_array('SQL', $flags ) ) {
            $input = $this->sanitize_sql_string( $input, $min, $max );
        }
        if ( $flags == 'LDAP' || in_array('LDAP', $flags ) ) {
            $input = $this->sanitize_ldap_string( $input, $min, $max );
        }
        if ( $flags == 'SYSTEM' || in_array('SYSTEM', $flags ) ) {
            $input = $this->sanitize_system_string( $input, $min, $max );
        }
        return $input;
    }

    /*
     * private my_utf8_decode method
     * @param string $string
     * @return string
     *
     * Internal function for utf8 decoding
     * thanks to Jamie Pratt for noticing that PHP's function is a little
     * screwy
     */
    private function my_utf8_decode( $string )
    {
        return strtr($string,
            "???????Â¥ÂµÃ€ÃÃ‚ÃƒÃ„Ã…Ã†Ã‡ÃˆÃ‰ÃŠÃ‹ÃŒÃÃŽÃÃÃ‘Ã’Ã“Ã”Ã•Ã–Ã˜Ã™ÃšÃ›ÃœÃÃŸÃ Ã¡Ã¢Ã£Ã¤Ã¥Ã¦Ã§Ã¨Ã©ÃªÃ«Ã¬Ã­Ã®Ã¯Ã°Ã±Ã²Ã³Ã´ÃµÃ¶Ã¸Ã¹ÃºÃ»Ã¼Ã½Ã¿",
            "SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy");
    }

    /**
     * private sanitize_paranoid_string
     * @param $string, int $min, int $max
     * @return bool|mixed
     */
    private function sanitize_paranoid_string( $string, $min = 0, $max = 999999 )
    {
        $string = preg_replace("/[^a-zA-Z0-9]/", "", $string);
        $len = strlen($string);
        if ((($min != 0) && ($len < $min)) || (($max != 999999 ) && ($len > $max))) {
            return false;
        }
        return $string;
    }

    /*
     * private sanitize_system_string method
     * @param string $string, int $min, int $max
     * @return string
     *
     * sanitize a string in prep for passing a single argument to system() (or similar)
     */
    private function sanitize_system_string( $string, $min = 0, $max = 999999 )
    {
        $pattern = '/(;|\||`|>|<|&|^|"|' . "\n|\r|'" . '|{|}|[|]|\)|\()/i'; // no piping, passing possible environment variables ($),

        // seperate commands, nested execution, file redirection,
        // background processing, special commands (backspace, etc.), quotes
        // newlines, or some other special characters
        $string = preg_replace( $pattern, '', $string );
        $string = '"' . preg_replace( '/\$/', '\\\$', $string ) . '"'; //make sure this is only interpretted as ONE argument

        $len = strlen( $string );

        if ( ( ( $min != 0 ) && ( $len < $min ) ) || ( ( $max != 999999 ) && ( $len > $max ) ) ) {
            return false;
        }
        return $string;
    }

    /*
     * private sanitize_sql_string method
     * @param string $string, int $min, int $max
     * @return string
     *
     * sanitize a string for SQL input (simple slash out quotes and slashes)
     */
    private function sanitize_sql_string( $string, $min = 0, $max = 999999  )
    {
        $pattern[0] = '/(\\\\)/';
        $pattern[1] = "/\"/";
        $pattern[2] = "/'/";
        $replacement[0] = '\\\\\\';
        $replacement[1] = '\"';
        $replacement[2] = "\\'";
        $len = strlen($string);
        if ( ( ( $min != 0 ) && ( $len < $min ) ) || ( ( $max != 999999 ) && ( $len > $max ) ) ) {
            return false;
        }
        return preg_replace( $pattern, $replacement, $string );
    }

    /*
     * private sanitize_ldap_string method
     * @param string $string, string $min, string $max
     * @return string
     *
     * sanitize a string for SQL input (simple slash out quotes and slashes)
     */
    private function sanitize_ldap_string( $string, $min = 0, $max = 999999 )
    {
        $pattern = '/(\)|\(|\||&)/';
        $len = strlen( $string );
        if ( ( ( $min != 0 ) && ( $len < $min ) ) || ( ( $max != 999999 ) && ( $len > $max ) ) ) {
            return false;
        }
        return preg_replace( $pattern, '', $string );
    }

    /*
     * private sanitize_html_string method
     * @param string $string
     * @return string
     *
     * sanitize a string for HTML (make sure nothing gets interpreted!)
     */
    private function sanitize_html_string( $string )
    {
        $pattern[0] = '/\&/';
        $pattern[1] = '/</';
        $pattern[2] = "/>/";
        $pattern[3] = '/\n/';
        $pattern[4] = '/"/';
        $pattern[5] = "/'/";
        $pattern[6] = "/%/";
        $pattern[7] = '/\(/';
        $pattern[8] = '/\)/';
        $pattern[9] = '/\+/';
        $pattern[10] = '/-/';
        $replacement[0] = '&amp;';
        $replacement[1] = '&lt;';
        $replacement[2] = '&gt;';
        $replacement[3] = '<br>';
        $replacement[4] = '&quot;';
        $replacement[5] = '&#39;';
        $replacement[6] = '&#37;';
        $replacement[7] = '&#40;';
        $replacement[8] = '&#41;';
        $replacement[9] = '&#43;';
        $replacement[10] = '&#45;';
        return preg_replace( $pattern, $replacement, $string );
    }

    /*
     * private sanitize_int method
     * @param mixed $integer, int $min, int $max
     * @return int
     *
     * make int int!
     */
    private function sanitize_int( $integer, $min = 0, $max = 999999 )
    {
        $int = intval( $integer );
        if ( ( ( $min != 0 ) && ( $int < $min ) ) || ( ( $max != 999999 ) && ( $int > $max ) ) ) {
            return false;
        }
        return $int;
    }

    /*
     * private sanitize_float method
     * @param mixed $float, int $min, int $max
     * @return float
     *
     * make float float!
     */
    private function sanitize_float( $float, $min = 0, $max = 999999 )
    {
        $float = floatval( $float );
        if ( ( ( $min != 0 ) && ( $float < $min ) ) || ( ( $max != 999999 ) && ( $float > $max ) ) ) {
            return false;
        }
        return $float;
    }

}