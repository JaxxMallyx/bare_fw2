<?php

namespace base\models;

class OverviewModel {

    public $overviewArr;

    public $overviewHead;

    public $overviewRows;

    public $overviewTitle;

    public $idColumn;

    public $options;

    /*
     * public set_overviewArr method
     * @param array $overviewArr
     */
    public function set_overviewArr( array $overviewArr )
    {
        $this->overviewArr = $overviewArr;
    }

    /*
     * public set_overviewHead method
     * @param array $overviewHead
     */
    public function set_overviewHead( array $overviewHead )
    {
        $this->overviewHead = $overviewHead;
    }

    /*
     * public set_overviewRows method
     * @param array $overviewRows
     */
    public function set_overviewRows( array $overviewRows )
    {
        $this->overviewRows = $overviewRows;
    }

    /*
     * public set_overviewTitle method
     * @param string $overviewTitle
     */
    public function set_overviewTitle( string $overviewTitle )
    {
        $this->overviewTitle = $overviewTitle;
    }

    /*
     * public set_idColumn method
     * @param string $idColumn
     */
    public function set_idColumn( string $idColumn )
    {
        $this->idColumn = $idColumn;
    }

    public function set_options( array $options )
    {
        if ( $this->idColumn ) $this->options = $options;
        else throw new \Exception( 'id column needs to be set for edit options to work.' );
    }

}