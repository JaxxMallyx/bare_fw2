<?php

namespace base\models;

class UserModel {

    public $userId;

    protected $username;

    private $email;

    private $password;

    private $hash;

    private $hashArr;

    private $firstname;

    private $prefix;

    private $lastname;

    private $roleId;

    private $level;

    /*
     * protected set_userId method
     * @param int $userId
     */
    protected function set_userId( int $userId )
    {
        $this->userId = $userId;
    }

    /*
     * protected get_userId method
     * @return int
     */
    public function get_userId()
    {
        return $this->userId;
    }

    /*
     * protected set_username method
     * @param string $username
     */
    protected function set_username( string $username )
    {
        $this->username = $username;
    }

    /*
     * protected get_username method
     * @return string
     */
    public function get_username()
    {
        return $this->username;
    }

    /*
     * protected set_email method
     * @param string $email
     */
    protected function set_email( string $email )
    {
        $this->email = $email;
    }

    /*
     * protected get_email method
     * @return string
     */
    public function get_email()
    {
        return $this->email;
    }

    /*
     * protected set_password method
     * @param string $password
     */
    protected function set_password( string $password )
    {
        $this->password = $password;
    }

    /*
     * protected get_password method
     * @return string
     */
    protected function get_password()
    {
        return $this->password;
    }

    /*
     * protected set_hash method
     * @param string $hash
     */
    protected function set_hash( string $hash )
    {
        $this->hash = $hash;
    }

    /*
     * protected get_hash method
     * @return string
     */
    protected function get_hash()
    {
        return $this->hash;
    }

    /*
     * protected set_hashArr method
     * @param array $hash
     */
    protected function set_hashArr( $hash )
    {
        if ( is_array( $hash ) ) $this->hashArr = $hash;
        else $this->hashArr[] = $hash;
    }

    /*
     * protected get_hashArr method
     * @return mixed
     */
    protected function get_hashArr( $key = null )
    {
        if ( is_int( $key ) ) return $this->hashArr[$key];
        else return $this->hashArr;
    }

    /*
     * protected set_firstname method
     * @param string $firstname
     */
    protected function set_firstname( string $firstname )
    {
        $this->firstname = $firstname;
    }

    /*
     * protected get_firstname method
     * @return string
     */
    public function get_firstname()
    {
        return $this->firstname;
    }

    /*
     * protected set_prefix method
     * @param string $prefix
     */
    protected function set_prefix( string $prefix )
    {
        $this->prefix = $prefix;
    }

    /*
     * protected get_prefix method
     * @return string
     */
    protected function get_prefix()
    {
        return $this->prefix;
    }

    /*
     * protected set_lastname method
     * @param string $lastname
     */
    protected function set_lastname( string $lastname )
    {
        $this->lastname = $lastname;
    }

    /*
     * protected get_lastname method
     * @return string
     */
    protected function get_lastname()
    {
        return $this->lastname;
    }

    /*
     * protected set_roleId method
     * @param int $roleId
     */
    protected function set_roleId( int $roleId = 0 )
    {
        $this->roleId = $roleId;
    }

    /*
     * protected get_roleId method
     * @return int
     */
    public function get_roleId()
    {
        return $this->roleId;
    }

    /*
     * protected set_level method
     * @param int $level
     */
    protected function set_level( int $level )
    {
        $this->level = $level;
    }

    /*
     * public get_level method
     * @return int
     */
    public function get_level()
    {
        return $this->level;
    }
}