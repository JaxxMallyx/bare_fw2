<?php

namespace base\models;

class ApplicationModel {

    public const STATE = 'DEVELOPMENT'; // PRODUCTION | DEVELOPMENT

    public $siteUrl;

    public $siteTitle;

    public $siteSubTitle;

    public $siteDescription;

    public $routObj;

    public $userObj;

    public $pageObj;

    /*
     * protected set_dbSettings method
     * @param array $dbSettings
     */
    protected function set_dbSettings( array $dbSettings )
    {
        define( 'DBHOST', $dbSettings['DB_HOST'] );
        define( 'DBNAME', $dbSettings['DB_NAME'] );
        define( 'DBUSER', $dbSettings['DB_USER'] );
        define( 'DBPASS', $dbSettings['DB_PASS'] );
        define( 'AES', $dbSettings['AES'] );
    }

    /*
     * protected set_siteUrl method
     * @param string $siteUrl
     */
    protected function set_siteUrl ( string $siteUrl )
    {
        $this->siteUrl = $siteUrl;
    }

    /*
     * protected get_siteUrl
     * @return string
     */
    public function get_siteUrl ()
    {
        return $this->siteUrl;
    }

    /*
     * protected set_siteTitle method
     * @param string $siteTitle
     */
    protected function set_siteTitle ( string $siteTitle )
    {
        $this->siteTitle = $siteTitle;
    }

    /*
     * protected get_siteTitle
     * @return string
     */
    public function get_siteTitle ()
    {
        return $this->siteTitle;
    }

    /*
     * protected set_siteSubTitle method
     * @param string $siteSubTitle
     */
    protected function set_siteSubTitle ( string $siteSubTitle )
    {
        $this->siteSubTitle = $siteSubTitle;
    }

    /*
     * protected get_siteSubTitle
     * @return string
     */
    public function get_siteSubTitle ()
    {
        return $this->siteSubTitle;
    }

    /*
     * protected set_siteDescription method
     * @param string $siteDescription
     */
    protected function set_siteDescription ( string $siteDescription )
    {
        $this->siteDescription = $siteDescription;
    }

    /*
     * protected get_siteDescription method
     * @return string
     */
    public function get_siteDescription ()
    {
        return $this->siteDescription;
    }

    /*
     * public set_appRouter method
     * @param object $routeObj
     */
    public function set_routObj()
    {
        $this->routObj = new \base\controllers\RouteController( $this->get_siteUrl() );
    }

    /*
     * public get_appRouter method
     * @return object
     */
    public function get_routObj()
    {
        return $this->routObj;
    }

    /*
     * public set_appRouter method
     * @param object $routeObj
     */
    public function set_userObj()
    {
        $this->userObj = new \base\controllers\UserController();
        if ( !$this->userObj->set_user_by_session() ) $this->userObj = false;
    }

    /*
     * public get_appRouter method
     * @return object
     */
    public function get_userObj()
    {
        return $this->userObj;
    }

    /*
     * protected set_pageObj method
     * @param void
     */
    protected function set_pageObj()
    {
        $this->pageObj = new \base\controllers\PageController( $this->get_routObj(), $this->get_userObj() );
    }

    /*
     * protected get_pageObj method
     * 2return object
     */
    protected function get_pageObj()
    {
        return $this->pageObj;
    }


}