<?php

namespace base\models;

class RouteModel {

    /*
     * Standard route array should be defined as shown below:
     * array(
     * [routeName*] => array(
     *      'url' => [alternative route],
     *      'title' => [route title],
     *      'vars' => array(
     *          [var key] => [var type]
     *          )
     *       );
     * *routeName also serves as pageUrl
     * Allowed variable types: bool, int, float, string.
     */
    protected $routes = array(
        'home'   => array(
            'url'       => '',
            'title'     => 'Home',
            'vars'      => array()
        ),
        '404'       => array(
            'title'     => 'Not found',
            'vars'      => array()
        ),
        'login'     => array(
            'title'     => 'Login',
            'vars'      => array(
                'state'     => 'string'
            )
        ),
        'logout'    => array(
            'title'     => 'Logout',
            'vars'      => array()
        ),
        'register'   => array(
            'title'     =>'Register',
            'vars'      => array(
            )
        ),
        'adminpanel' => array(
            'title'     => 'Admin Panel',
            'auth'      => true,
            'vars'      => array(
                'page_title' => 'string',
                'detail_id'  => 'int'
            )
        ),
        'ajax' => array(
            'title'     => 'ajax',
            'vars'      => array(
                'obj'       => '*string',
                'func'      => '*string',
                'vars'      => 'string'
            )
        )
    );

    public $requestUrl;

    public $pageUrl;

    public $pageTitle;

    public $altTitle;

    public $pageVars;

    /*
     * public add_route method
     * @param string $title, string $url, string $handle
     */
    public function add_route( string $title, string $url, bool $auth = false, $handle = '' )
    {
        $newRoute = array( 'title' => $title, 'auth' => $auth, 'vars' => array() );

        if ( !empty( $handle ) ) {
            $newRoute['url'] = $url;
            $this->routes[$handle] = $newRoute;
        } else {
            $this->routes[$url] = $newRoute;
        }
    }

    /*
     * protected get_route method
     * Returns route by array key
     * @return array, bool
     */
    protected function get_route( string $routeName = '', string $key = '')
    {

        if ( isset( $this->routes[$routeName] ) ) {

            switch ( $key ) {
                case 'routename':
                    return $routeName;
                    break;
                case 'url':
                    return ( isset( $this->routes[$routeName]['url'] ) ? $this->routes[$routeName]['url'] : $routeName );
                    break;
                case 'title':
                    return $this->routes[$routeName]['title'];
                    break;
                case 'auth':
                    return ( isset( $this->routes[$routeName]['auth'] ) ? $this->routes[$routeName]['auth'] : false );
                    break;
                case 'vars':
                    return $this->routes[$routeName]['vars'];
                    break;
                default:
                    return array($this->routes[$routeName], 'routename' => $routeName);
                    break;
            }

        } else if ( $routeKey = $this->search_routes_by_value( $routeName, 'url' ) ) {
            return $this->get_route( $routeKey, $key );
        } else {
            if ( \base\models\ApplicationModel::STATE == 'DEVELOPMENT' ) throw new \Exception('No such route. Please define in \base\model\RouteModel.');
            else $this->go_to( '404' );
        }
    }

    /*
     * private search_routes_by_value method
     * @param string $var, string $key
     * return mixed
     */
    private function search_routes_by_value( string $var = '', string $key = '' )
    {
        foreach( $this->routes as $routename => $route ) {
            if ( isset( $route[$key] ) && $route[$key] == $var ) return $routename;
        }
        return false;
    }

    /*
     * protected get_currect_route method
     * @param string $key
     */
    public function get_current_route( string $key = '' )
    {
        return $this->get_route( $this->pageUrl, $key );
    }

    /*
     * protected set_requestUrl method
     * @param string $requestUrl
     */
    protected function set_requestUrl( string $requestUrl )
    {
        $this->requestUrl = filter_var( htmlspecialchars( $requestUrl ), FILTER_SANITIZE_URL );
    }

    /*
     * protected set_baseUrl method
     * @param string $baseUrl
     */
    protected function set_baseUrl( string $baseUrl )
    {
        if ( !defined( 'BASEURL' ) ) define( 'BASEURL', $baseUrl );
    }

    /*
     * protected set_props_by_route method
     * @return void
     */
    protected function set_props_by_route( string $routeName = '' )
    {
        $this->set_pageUrl( $this->get_route( $routeName, 'url' ) );
        $this->set_pageTitle( $this->get_route( $routeName, 'title' ) );
    }

    /*
     * protected set_pageTitle method
     * @return void
     */
    public function set_pageTitle( string $pageTitle = '' )
    {
        $this->pageTitle = $pageTitle;
    }

    /*
     * protected set_pageTitle method
     * @return void
     */
    public function set_altTitle( string $altTitle = '' )
    {
        $this->altTitle = $altTitle;
    }

    /*
     * protected set_pageUrl method
     * @return void
     */
    protected function set_pageUrl( string $pageUrl = '' )
    {
        $this->pageUrl = $pageUrl;
    }

    /*
     * protected set_pageVars method
     * @return void
     */
    protected function set_pageVar( string $key = '', $val )
    {
        if ( !empty( $key ) ) $this->pageVars[$key] = $val;
        else $this->pageVars[] = $val;
    }

    /*
     * protected set_pageVars method
     * @return void
     */
    public function get_pageVars( string $key = '' )
    {
        if ( empty( $key ) ) return $this->pageVars;
        elseif ( isset($this->pageVars[$key]) ) return $this->pageVars[$key];
        else return false;
    }

    /*
     * public static get_baseDir method
     * @return string
     */
    public static function get_baseDir()
    {
        return $_SERVER['DOCUMENT_ROOT'] . dirname($_SERVER['SCRIPT_NAME']) . '/';
    }

}

