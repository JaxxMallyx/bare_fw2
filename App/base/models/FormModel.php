<?php

namespace base\models;

class FormModel {

    public $formElement;

    public $inputArr;

    private $formValues;

    private $rawValues;

    private $formFeedback;

    /*
     * protected set_formElement method
     * @param string $formElement
     */
    protected function set_formElement( string $formElement )
    {
        $this->formElement = $formElement;
    }

    /*
     * protected set_inputArr method
     * @param string $key, string $value, string $group
     */
    protected function set_inputArr( string $key, string $value, $group = null )
    {
        if ( !$group ) $this->inputArr[$key] = $value;
        else $this->inputArr[$group][$key] = $value;
    }

    /*
     * public set_formValues method
     * @param string $key, mixed $value
     */
    protected function set_formValues( string $key, $value )
    {
        $this->formValues[$key] = $value;
    }

    /*
     * protected get_formValues method
     * @param string $key, bool $all
     * return mixed
     */
    public function get_formValues( $key = '', $all = true )
    {
        if ( $key && isset( $this->formValues[$key] ) ) return $this->formValues[$key];
        elseif ( $all ) return $this->formValues;
        else return false;
    }

    /*
     * public set_rawValues method
     * @param string $key, string $value
     */
    protected function set_rawValues( string $key, $value )
    {
        $this->rawValues[$key] = $value;
    }

    /*
     * protected get_rawValues method
     * @param string $key
     * return string
     */
    protected function get_rawValues( $key = '' )
    {
        if ( $key && isset( $this->rawValues[$key] ) ) return $this->rawValues[$key];
        else return false;
    }

    /**
     * protected set_formFeedback method
     * @param string $feedback
     */
    public function set_formFeedback( string $feedback = '' )
    {
        $this->formFeedback = $feedback;
    }

    /**
     * protected get_formFeedback method
     * @return string
     */
    protected function get_formFeedback(  )
    {
        return $this->formFeedback;
    }

}