<?php

namespace base\models;

class PageModel {

    public $routeObj;

    public $userObj;

    public $menuObj;

    protected $dependencies;

    protected $defPageObj;

    private $baseUrl;

    private $roles;

    private $user;


    /*
     * protected set_routeObj method
     * @param object $routeObj
     */
    protected function set_routeObj( object $routeObj )
    {
        $this->routeObj = $routeObj;
    }

    /*
     * protected get_routeObj method
     * @return object
     */
    protected  function get_routeObj()
    {
        return $this->routeObj;
    }

    /*
     * protected set_userObj method
     * @param object,boolean $userObj
     */
    protected function set_userObj( $userObj )
    {
        $this->userObj = $userObj;
    }

    /*
     * protected get_userObj method
     * @return object
     */
    protected  function get_userObj()
    {
        return $this->userObj;
    }

    /*
     * protected set_pageUrl method
     * @param string $pageUrl
     */
    protected function set_pageUrl( string $pageUrl )
    {
        $this->pageUrl = $pageUrl;
    }

    /*
     * protected set_pageTitle method
     * @param string $pageTitle
     */
    public function set_pageTitle( string $pageTitle )
    {
        $this->pageTitle = $pageTitle;
    }

    /*
     * protected set_pageVars method
     * @param array $pageVars
     */
    protected function set_pageVars( $pageVars = array() )
    {
        $this->pageVars = $pageVars;
    }

    /*
     * protected set_menuObj method
     * @param object $menuObj
     */
    protected  function set_menuObj( object $menuObj )
    {
        $this->menuObj = $menuObj;
    }

    /*
     * protected set_dependencies method
     * @param string $dest, string $file, string $type, string $name, string $dep
     */
    public function set_dependencies( string $dest = 'footer', string $file, string $type, $subDir = '', string $name = null, string $dep = null )
    {
        if ( empty( $this->dependencies ) ) {
            $this->dependencies = array(
                'header' => array(),
                'footer' => array()
            );
        }

        switch ( $type ) {
            case 'css':
                if ( !$subDir ) $subDir = 'css';
                $linkStr = '<link rel="stylesheet" type="text/css" href="' . BASEURL . 'Public/src/style/' . $subDir . '/[src].' . $type .'" [name]>';
                break;
            case 'js':
                $linkStr = '<script type="text/javascript" src="' . BASEURL . 'Public/src/js[sub]/[src].' . $type .'" [name]></script>';
                if ( $subDir ) $linkStr = str_replace( '[sub]', '/' . $subDir, $linkStr );
                else $linkStr = str_replace( '[sub]', '', $linkStr );
                break;
        }

        if ( isset( $linkStr ) ) {
            $linkStr = str_replace( '[src]', $file, $linkStr );

            if ( $name !== null ) {
                $linkStr = str_replace( '[name]', 'data-name="' . $name . '"', $linkStr );
                $key = $name;
            } else {
                $linkStr = str_replace( '[name]', '', $linkStr );
            }

            $key = ( !empty( $key ) ? $key : $file );

            if ( $dep ) {
                $arrKeysHead = array_keys( $this->dependencies['header'] );
                $arrSrchHead = array_search( $dep, $arrKeysHead );
                $arrKeysFoot = array_keys( $this->dependencies['footer'] );
                $arrSrchFoot = array_search( $dep, $arrKeysFoot );

                if ( $arrSrchHead ) array_splice($this->dependencies['header'], $arrSrchHead + 1, 0, $linkStr);
                if ( $arrSrchFoot ) array_splice($this->dependencies['footer'], $arrSrchFoot + 1, 0, $linkStr);
            } else {
                $this->dependencies[$dest][$key] = $linkStr;
            }
        }
    }

    /*
     * public get_dependencies method
     * @param string $dest
     * @return string
     */
    public function get_dependencies( string $dest )
    {
        if ( !empty( $this->dependencies ) )  return implode( '', $this->dependencies[ $dest ] );
        else return null;
    }

    /*
     * protected set_defPageObj method
     * @param object $pageObj
     */
    protected function set_defPageObj( object $defPageObj )
    {
        $this->defPageObj = $defPageObj;
    }

    /*
     * protected get_defPageObj method
     * @return object
     */
    protected function get_defPageObj()
    {
        return $this->defPageObj;
    }
}