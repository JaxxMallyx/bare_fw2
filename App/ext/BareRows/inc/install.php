<?php

namespace ext\BareRows\inc;

class Install
{

    public $installLog;

    public $error = false;

    public function __construct()
    {
        $installLog = 'Install started...<br><hr>';
        $installLog .= $this->create_tables();
        $installLog .= $this->insert_option_categories();

        $this->installLog = $installLog;
    }

    /*
     * private create_tables method
     * @return string
     */
    private function create_tables()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $createLog = '<hr>Create tables... <br>';

        $db->query('CREATE TABLE IF NOT EXISTS br_rows (
                                          row_id INT(11) NOT NULL AUTO_INCREMENT, 
                                          page_id INT(11) NOT NULL,
                                          options VARCHAR(255) NULL,
                                        PRIMARY KEY (row_id)
                                      ) ENGINE = InnoDB;
                          ALTER TABLE br_rows 
                          ADD CONSTRAINT page_id FOREIGN KEY (page_id) 
                          REFERENCES page(page_id) ON DELETE CASCADE ON UPDATE CASCADE;');
        if ( $db->execute() ) {
            $createLog .= '- br_rows created! <br>';
        } else {
            $this->error = true;
            $createLog .= '&lt;!&gt; Failed to create br_rows.<br>';
        }

        $db->query('CREATE TABLE IF NOT EXISTS br_columns (
                                          col_id INT(11) NOT NULL AUTO_INCREMENT,
                                          row_id INT(11) NOT NULL,
                                          title VARCHAR(150) NULL,
                                          name VARCHAR(150) NOT NULL,
                                          bg_type ENUM("image","color") NOT NULL DEFAULT "color",
                                          bg_value VARCHAR(100) NOT NULL DEFAULT "white",
                                          content TEXT NULL, 
                                          options VARCHAR(255) NULL, 
                                        PRIMARY KEY (col_id)
                                      ) ENGINE = InnoDB;
                            ALTER TABLE br_columns 
                            ADD CONSTRAINT row_id FOREIGN KEY (row_id) 
                            REFERENCES br_rows(row_id) ON DELETE CASCADE ON UPDATE CASCADE;');
        if ( $db->execute() ) {
            $createLog .= '- br_rows created! <br>';
        } else {
            $this->error = true;
            $createLog .= '&lt;!&gt; Failed to create br_rows.<br>';
        }

        $db->query('CREATE TABLE IF NOT EXISTS br_options (
                                          option_id INT(11) NOT NULL AUTO_INCREMENT,
                                          parent_id INT(11) NULL,
                                          br_key VARCHAR(30) NOT NULL, 
                                          br_value VARCHAR(100) NOT NULL, 
                                        PRIMARY KEY (option_id)
                                      ) ENGINE = InnoDB;');
        if ( $db->execute() ) {
            $createLog .= '- br_options created! <br>';
        } else {
            $this->error = true;
            $createLog .= '&lt;!&gt; Failed to create br_options.<br>';
        }

        $createLog .= '<hr>';

        return $createLog;

    }

    private function insert_option_categories()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $insertLog = '<hr>Insert option categories...<br>';

        $db->query('SELECT count(option_id) AS count FROM `br_options` WHERE br_key = "category" AND br_value IN ( "Content Class", "Attributes", "Animation Class");');

        if ( $db->execute() && isset($db->single()['count']) && (int)$db->single()['count'] < 3 ) {
            $db->query('INSERT IGNORE INTO br_options (br_key, br_label, br_value) VALUES 
                                  ("category", "Content Class", "content_class"),
                                  ("category", "Attributes", "attributes"),
                                  ("category", "Animation Class", "animation_class")');

            if ($db->execute()) {
                $insertLog .= 'Categories inserted<br>';
            } else {
                $this->error = true;
                $insertLog .= '&lt;!&gt; Failed to insert the categories';
            }

            $insertLog .= '<hr>';

        } else {
            $insertLog .= '&lt;!&gt; Categories already present in the database.';
        }

        $insertLog .= '<hr>';

        return $insertLog;
    }
}