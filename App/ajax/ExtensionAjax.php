<?php

namespace ajax;

class ExtensionAjax extends \ajax\AjaxAbstract
{
    public function __construct($pageObj)
    {
        $this->init_ajaxFunc($pageObj, 0);
    }

    static protected function install_extension( $installVar )
    {
        $extFile = \base\controllers\ApplicationController::load_file('extmain.php', 'App\ext', $installVar, true);

        if ( $extFile ) {
            include $extFile;

            $extObjStr = '\ext\\' . $installVar . '\\' . $installVar;
            $extObj = new $extObjStr();
            $installArr = $extObj->install_extension();

            echo $installArr[1];

            if (!$installArr[0]) self::add_extension($installVar, $extFile);
        } else {
            echo 'No "extmain.php" file found or no directory by the name: "' . $installVar . '" found';
        }

    }

    static protected function add_extension( $installVar )
    {
        $db = \base\controllers\ApplicationController::get_db();
        $extObj = \base\controllers\ExtensionController::new_ext_by_dir( $installVar );

        $db->query('SELECT extension_id FROM extensions WHERE extension_title = :title');
        $db->bind(':title', $installVar);

        if ( $db->execute() && !$db->single() ) {
            $db->query('INSERT INTO extensions(
                                            extension_title, 
                                            extension_dir, 
                                            description,
                                            version, 
                                            active
                                          ) VALUES (
                                            :title,
                                            :dir,
                                            :description,
                                            :version,
                                            :active )');
            $db->bind(':title', $extObj->title);
            $db->bind(':dir', $extObj->dir);
            $db->bind(':description', $extObj->description);
            $db->bind(':version', $extObj->version);
            $db->bind(':active', $extObj->active);

            if ($db->execute() && $db->lastInsertId()) {
                echo '<hr>Extension added under id: ' . $db->lastInsertId() . '. You can activate it now!<hr>';
            } else {
                echo '<hr>Something went wrong when adding the extension to the dedicated table.<hr>';
            }
        } else {
            echo '<hr>Extension already exists in the database.<hr>';
        }
    }

    static protected function activate_extension( $extensionId )
    {
        $db = \base\controllers\ApplicationController::get_db();
        $db->query('UPDATE extensions SET active = 1 WHERE extension_id = :id' );
        $db->bind(':id', $extensionId);
        if ($db->execute()) {
            echo '1';
        } else {
            echo '0';
        }
    }

}