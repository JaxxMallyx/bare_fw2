<?php

namespace ajax;

abstract class AjaxAbstract
{

    public $pageObj;

    public $ajaxFunc;

    public $functionVars;

    public function init_ajaxFunc( object $pageObj, $authLevel = null )
    {
        $this->pageObj = $pageObj;

        if ( $authLevel !== null && $this->pageObj->check_level( $authLevel ) == false ) exit;

        $this->get_function();
        $this->format_functionVars();
        $this->run_func();
    }

    private function get_function()
    {
        $this->ajaxFunc = $this->pageObj->routeObj->pageVars['func'];
    }

    private function format_functionVars()
    {
        $functionVars = array();

        if (isset($this->pageObj->routeObj->pageVars['vars'])) {
            $explArr = explode('&amp;', $this->pageObj->routeObj->pageVars['vars']);

            foreach ($explArr as $value) {
                if ($value != '') {
                    $explVars = explode('=', $value);
                    $key = filter_var($explVars[0], FILTER_SANITIZE_STRING);
                    $var = $explVars[1];
                    //print_r($var);
                    if (is_numeric($var)) {
                        $var = floatval(filter_var($var, FILTER_SANITIZE_NUMBER_FLOAT));
                        if ((int)$var == $var) {
                            $var = (int)$var;
                        }
                    } else {
                        $var = filter_var($var, FILTER_SANITIZE_STRING);
                    }
                    $functionVars[$key] = urldecode($var);
                }
            }
        }
        $this->functionVars = $functionVars;
    }

    private function run_func()
    {
        $funcName = $this->ajaxFunc;
        $objName = ucfirst( $this->pageObj->routeObj->pageVars['obj'] ) . 'Ajax';
        call_user_func_array( array( '\ajax\\' . $objName, $funcName ), $this->functionVars );
    }

    // @ToDo: Figure out a way to parse function vars and automate function execution
}