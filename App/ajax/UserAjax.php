<?php

namespace ajax;

class UserAjax extends \ajax\AjaxAbstract {

    public function __construct( $pageObj )
    {
        $this->init_ajaxFunc( $pageObj, 0 );
    }

    static protected function add_user()
    {
//        echo 'SELECT
//                                user_id,
//                                AES_DECRYPT(username, "' . AES . '") AS username,
//                                AES_DECRYPT(email, "' . AES . '") AS email,
//                                AES_DECRYPT(firstname, "' . AES . '") AS firstname,
//                                AES_DECRYPT(prefix, "' . AES . '") AS prefix,
//                                AES_DECRYPT(lastname, "' . AES . '") AS lastname,
//                                role_id,
//                                role.title AS role_title
//                            FROM user
//                            LEFT JOIN user_role AS role USING(role_id)';
//        exit;
        $roles = self::get_all_roles();
        $formObj = new \base\controllers\FormController( 'edit-user', 'post', array( 'ajax' => array( 'obj' => 'user', 'func' => 'insert_user', 'callback' => 'reload' ) ) );
        $formObj->set_input('text', 'username', array( 'required' => true ) );
        $formObj->set_input('email', 'email', array( 'required' => true ) );
        $formObj->set_input('password', 'password', array( 'required' => true ) );
        $formObj->set_input('text', 'firstname', array( 'required' => true ) );
        $formObj->set_input('text', 'prefix' );
        $formObj->set_input('text', 'lastname', array( 'required' => true ) );
        $formObj->set_input( 'select', 'role_id', array( 'values' => $roles, 'selected' => 3 ) );
        $formObj->set_input('submit', 'Add user' );
        echo $formObj->generate_form();
    }

    static protected function insert_user( $username, $email, $password, $firstname, $prefix = null, $lastname, $roleId )
    {
        $sanitize = new \base\lib\Sanitize();
        $username = ( $username ? $sanitize->input( $username, array( 'STRING', 'UTF8' ) ) : false );
        $email = ( $email ? $sanitize->input( $email, array( 'STRING', 'UTF8' ) ) : false );
        $password = ( $password ? $sanitize->input( $password, array( 'STRING', 'UTF8' ) ) : false );
        $firstname = ( $firstname ? $sanitize->input( $firstname, array( 'STRING', 'UTF8' ) ) : false );
        $prefix = ( $prefix  ? $sanitize->input( $prefix , array( 'STRING', 'UTF8' ) ) : false );
        $lastname = ( $lastname ? $sanitize->input( $lastname, array( 'STRING', 'UTF8' ) ) : false );
        $roleId = ( $roleId  ? $sanitize->input( $roleId , array( 'INT', 'UTF8' ) ) : false );

        $user = new \base\controllers\UserController( 0, $username, $email, $password, $firstname, $prefix, $lastname, $roleId);

        if ( $user->register_user() ) {
            echo '1';
        } else {
            echo '0';
        }


    }

    static protected function edit_user( $userId )
    {
        $user = self::get_user_by_id( $userId );
    }

    static protected function update_user( int $userId, string $username, string $email, int $roleId )
    {
        $db = \base\controllers\ApplicationController::get_db();
        $sanitize = new \base\lib\Sanitize();
        $userId = $sanitize->input($userId, array('INT', 'UTF8'));
        $username = $sanitize->input($username, array('STRING', 'UTF8'));
        $email = $sanitize->input($email, array('STRING', 'UTF8'));
        $roleId = $sanitize->input($roleId, array('INT', 'UTF8'));

        $db->query( 'UPDATE user SET 
                              username = AES_ENCRYPT(:username, "' . AES . '"),
                              email = AES_ENCRYPT(:email, "' . AES . '"),
                              role_id = :roleId 
                            WHERE user_id = :userId');
        $db->bind( ':username', $username );
        $db->bind( ':email', $email );
        $db->bind( ':roleId', $roleId );
        $db->bind( ':userId', $userId );

        if ( $db->execute() ) {
            return true;
        } else {
            return false;
        }
    }

    static protected function delete_user( $userId )
    {
        $db = \base\controllers\ApplicationController::get_db();
        $sanitize = new \base\lib\Sanitize();
        $userId = $sanitize->input($userId, array('INT', 'UTF8'));

        $db->query( 'DELETE FROM user WHERE user_id = :userId' );
        $db->bind( ':userId', $userId );

        if ( $db->execute() ) {
            return true;
        } else {
            return false;
        }
    }

    static private function get_user_by_id( $userId )
    {
        //print_r('userid:'.$userId);
        $user = new \base\controllers\UserController();

        if ( $user->get_user_by( array( 'user_id' => $userId ) ) ) {
            self::generate_edit_form( $user );
        } else {
            return false;
        }
    }



    static private function generate_edit_form( $user )
    {
        $roles = self::get_all_roles();
        $formObj = new \base\controllers\FormController( 'edit-user', 'post', array( 'ajax' => array( 'obj' => 'user', 'func' => 'update_user' ) ) );
        $formObj->set_input( 'hidden', 'user_id', array('value'=>$user->get_userId()) );
        $formObj->set_input( 'text', 'username', array('value'=>$user->get_username()) );
        $formObj->set_input( 'text', 'email', array('value'=>$user->get_email()) );
        $formObj->set_input( 'select', 'role_id', array('values'=>$roles, 'selected' => $user->get_roleId()) );
        $formObj->set_input( 'submit', 'Edit' );
        echo $formObj->generate_form();
    }

    static private function get_all_roles( $userObj = false )
    {
        if ( !$userObj ) $userObj = new \base\controllers\UserController();

        $roleArr = $userObj->get_all_roles();
        $roles = array();
        foreach ( $roleArr as $role ) {
            $roles[$role['role_id']] = $role['title'];
        }

        return $roles;
    }
}