<?php

namespace admin\page;

class UsersAdmin extends \admin\page\AdminAbstract {

    public $userOverview;

    public $roleOverview;

    public function __construct( $pageObj )
    {
        $this->set_pageObj( $pageObj );
        $this->set_adminPageDescription( 'User overview, user editing and role editing can be done on this page.' );

        $userArr = $this->get_all_users();
        if ( $userArr ) {
            $overviewObj = new \base\controllers\OverviewController( $userArr, array( 'role_id' => null, 'role_title' => 'role' ), 'user', 'user_id', array('edit'=>true,'delete'=>true,'add'=>true) );
            $this->userOverview = $overviewObj->generate_overview();
        }

        $roleArr = $this->get_all_roles();
        if ( $roleArr ) {
            $overviewObj = new \base\controllers\OverviewController( $roleArr, array( 'description' => null ), 'role', 'role_id', array('edit'=>true,'delete'=>true,'add'=>true) );
            $this->roleOverview = $overviewObj->generate_overview();
        }
    }

    private function get_all_users()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query( 'SELECT 
                                user_id,
                                AES_DECRYPT(username, "' . AES . '") AS username,
                                AES_DECRYPT(email, "' . AES . '") AS email,
                                AES_DECRYPT(firstname, "' . AES . '") AS firstname,
                                AES_DECRYPT(prefix, "' . AES . '") AS prefix,
                                AES_DECRYPT(lastname, "' . AES . '") AS lastname,
                                role_id,
                                role.title AS role_title
                            FROM user
                            LEFT JOIN user_role AS role USING(role_id)' );

        if ( $db->execute() && $db->resultset() ) {
            return $db->resultset();
        } else {
            return false;
        }
    }

    private function get_all_roles()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query( 'SELECT * FROM user_role' );

        if ( $db->execute() && $db->resultset() ) {
            return $db->resultset();
        } else {
            return false;
        }
    }

}