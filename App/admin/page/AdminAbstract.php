<?php

namespace admin\page;

abstract class AdminAbstract {

    public $pageObj;

    public $adminPageDescription;

    public function set_pageObj( object $pageObj )
    {
        $this->pageObj = $pageObj;
        $this->pageObj->set_dependencies('footer', 'admin', 'css');
    }

    public function set_adminPageDescription( string $description )
    {
        $this->adminPageDescription = $description;
    }
}