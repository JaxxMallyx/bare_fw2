<?php

namespace admin\page;

use page;

class DashboardAdmin extends \page\PageAbstract {

    public function __construct( $pageObj )
    {
        $pageObj->adminPageDescription = 'Standard overview of the system statistics.';

    }

}