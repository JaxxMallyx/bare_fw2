<?php

namespace admin\page;

class ExtensionsAdmin extends \admin\page\AdminAbstract {

    public $extensions;

    public $detailExtObj;

    public $extContent;

    public $extOverview;

    public $extMessage;

    /*
     *
     */
    public function __construct( $pageObj )
    {
        $this->set_pageObj($pageObj);

        $this->get_db_ext();
        $this->check_ext_dir();

        $detailId = ( !empty($this->pageObj->routeObj->get_pageVars('detail_id')) ? $this->pageObj->routeObj->get_pageVars('detail_id') : false );

        if ( $detailId && $this->get_ext_by_id( $detailId ) ) {
            $this->pageObj->routeObj->set_altTitle( 'Extension: ' . $this->detailExtObj->title );
            $this->set_adminPageDescription($this->detailExtObj->description . ' [Version: ' . $this->detailExtObj->version . ']');
            $this->extContent = $this->detailExtObj->extObj->admin_content();
        } else {
            $this->set_adminPageDescription('Extensions overview. View and activate/deactivate extensions.');

            $this->extOverview = $this->generate_overview();
        }
    }

    /*
     * protected get_ext_by_id method
     * @param int $id
     * @return bool
     */
    protected function get_ext_by_id( int $id )
    {
        if ( $extObj = \base\controllers\ExtensionController::get_ext_by($this->extensions, 'id', $id) ) {
            $this->detailExtObj = $extObj;
            return true;
        } else {
            return false;
        }
    }

    /*
     * protected check_ext_dir method
     * @return void
     */
    protected function check_ext_dir()
    {
        $extDir = scandir(BASEDIR . '\App\ext');

        foreach ( $extDir as $extension ) {
            if ( !empty($extension) && $extension != '.' && $extension != '..' && $extension != 'ExtensionAbstract.php' ) {
                if ( $this->extensions != NULL && !\base\controllers\ExtensionController::get_ext_by($this->extensions, 'dir', $extension) ) $this->new_ext_by_dir( $extension );
            }
        }
    }

    /*
     * protected get_db_ext method
     * @return void
     */
    protected function get_db_ext()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT * FROM extensions' );

        if ( $db->execute() && $db->resultset() ) {
            $this->extensions = $this->format_extensions( $db->resultset() );
        } else {
            $this->extensions = false;
        }
    }

    /*
     * private format_extensions method
     * @param $extArr
     * @return void
     */
    private function format_extensions( $extArr )
    {
        $formatArr = array();

        foreach ( $extArr as $ext ) {
            $newExt = new \base\controllers\ExtensionController( $ext['extension_id'], $ext['extension_title'], $ext['extension_dir'], $ext['description'], $ext['version'], true, $ext['active'] );

            $formatArr[$ext['extension_id']] = $newExt;
        }

        return $formatArr;
    }

    /*
     * private format_extensions method
     * @param $extArr
     * @return void
     */
    private function new_ext_by_dir( $dir )
    {
        $this->extensions[] = \base\controllers\ExtensionController::new_ext_by_dir( $dir );
    }

    private function generate_overview()
    {
        $overviewArr = array();
        foreach ( $this->extensions as $dir => $extension ) {
            $overviewArr[] = array(
                'id'        => $extension->id,
                'dir'       => $extension->dir,
                'title'     => $extension->title,
                'version'   => $extension->version,
                'db'        => $extension->db,
                'active'    => $extension->active
            );
        }

        $overviewObj = new \base\controllers\OverviewController(
            $overviewArr,
            null,
            'extension',
            'id',
            array(
                'install'   => 'dir',
                'activate'  => 'dir',
                'delete'    => true,
                'idurl'     => $this->pageObj->routeObj->requestUrl
            ));

        return $overviewObj->generate_overview();exit;
    }
}